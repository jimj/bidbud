package guilds.disciples;

import guilds.disciples.GuildConstants.Rank;
import net.jimj.bidbud.auction.Auction;
import net.jimj.bidbud.auction.Bid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class DisciplesAuctionSettler {
    private record Settlement(Bid<BidContext> priceSetter, List<Bid<BidContext>> winners) {}

    private static final Bid<BidContext> NO_BID = new Bid<>("", "", 0, new BidContext(Rank.RAIDER));
    private final BidComparator bidComparator = new BidComparator();
    private final Random random = new Random();

    public SettledAuction settle(
            final Auction<BidContext> auction) {

        final List<Bid<BidContext>> bids = new ArrayList<>(auction.getBids());
        bids.sort(bidComparator);

        final Settlement settlement = settle(auction.getLoot().quantity(), new ArrayList<>(bids));

        return new SettledAuction(
                auction.getLoot(),
                bids,
                settlement.priceSetter(),
                settlement.winners());
    }

    private Settlement settle(
            final int quantity,
            final List<Bid<BidContext>> bids) {

        final List<Bid<BidContext>> winners = new ArrayList<>();

        List<Bid<BidContext>> potentialWinners = new ArrayList<>();
        while (winners.size() < quantity && !bids.isEmpty()) {
            potentialWinners = popEqual(bids);

            while (winners.size() < quantity && !potentialWinners.isEmpty()) {
                final Bid<BidContext> winner = choose(potentialWinners);
                winners.add(winner);
            }
        }

        // The price-setter is the 2nd highest bid.
        // When many instances of an item are for auction, the price-setter can be someone who was tied for 2nd-highest bid but lost the roll-off.
        final Bid<BidContext> priceSetter;
        if (!potentialWinners.isEmpty()) {
            priceSetter = potentialWinners.get(0);
        } else if (!bids.isEmpty()) {
            priceSetter = bids.get(0);
        } else {
            priceSetter = NO_BID;
        }

        final List<Bid<BidContext>> settlements = winners.stream()
                .map(winningBid -> BidProcessor.calculateFinalPrice(winningBid, priceSetter))
                .toList();

        return new Settlement(priceSetter, settlements);
    }

    private List<Bid<BidContext>> popEqual(
            final List<Bid<BidContext>> candidates) {

        final List<Bid<BidContext>> winners = new ArrayList<>();
        final Iterator<Bid<BidContext>> candidateIterator = candidates.iterator();
        final BidComparator bidComparator = new BidComparator();

        while (candidateIterator.hasNext()) {
            final Bid<BidContext> candidate = candidateIterator.next();
            if (winners.isEmpty()
                    || bidComparator.compare(winners.get(0), candidate) == 0) {
                winners.add(candidate);
                candidateIterator.remove();
            } else {
                break;
            }
        }

        return winners;
    }

    private Bid<BidContext> choose(
            final List<Bid<BidContext>> equalBids) {
        if (equalBids.size() == 0) {
            throw new IllegalStateException();
        }

        final int chosen = random.nextInt(equalBids.size());
        return equalBids.remove(chosen);
    }
}
