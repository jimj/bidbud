package guilds.disciples;

public record BidContext(GuildConstants.Rank bidderRank) {
}
