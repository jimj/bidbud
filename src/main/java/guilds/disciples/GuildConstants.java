package guilds.disciples;

import java.util.Optional;

public class GuildConstants {
    static final String SERVER = System.getenv().getOrDefault("DEV_SERVER", "557073540952883212");
    static final String AUCTION_CHANNEL = System.getenv().getOrDefault("DEV_CHANNEL", "583179696372580380");
    static final String CONTROL_CHANNEL = System.getenv().getOrDefault("DEV_CHANNEL", "583180999282786305");

    enum Rank {
        ALT(0, 1),
        BOX(100, 3),
        CRITICAL(Integer.MAX_VALUE, 3),
        RAIDER(Integer.MAX_VALUE, 1);

        private final int cap;
        private final int multiplier;

        Rank(final int cap, final int multiplier) {
            this.cap = cap;
            this.multiplier = multiplier;
        }

        static Optional<Rank> from(final String val) {
            for (final Rank rank : values()) {
                if (rank.name().equalsIgnoreCase(val)) {
                    return Optional.of(rank);
                }
            }

            return Optional.empty();
        }

        public int getCap() {
            return cap;
        }

        public int getMultiplier() {
            return multiplier;
        }
    }
}
