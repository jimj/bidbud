package guilds.disciples;

import net.jimj.bidbud.auction.Bid;
import net.jimj.bidbud.auction.Loot;

import java.util.List;

public record SettledAuction(Loot loot, List<Bid<BidContext>> bids, Bid<BidContext> priceSetter, List<Bid<BidContext>> settlements) {}
