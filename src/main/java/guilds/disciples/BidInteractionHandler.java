package guilds.disciples;

import io.vertx.core.json.JsonObject;
import net.jimj.bidbud.auction.Auctioneer;
import net.jimj.bidbud.auction.Bid;
import net.jimj.bidbud.auction.Loot;
import net.jimj.bidbud.auction.RunningAuction;
import net.jimj.discord.agent.UserPreferences;
import net.jimj.discord.agent.interactions.CommandInteraction;
import net.jimj.discord.agent.interactions.ComponentInteraction;
import net.jimj.discord.agent.interactions.InteractionContext;
import net.jimj.discord.agent.interactions.InteractionFacade;
import net.jimj.discord.agent.interactions.InteractionRegistry;
import net.jimj.discord.agent.interactions.ModalInteraction;
import net.jimj.discord.agent.interactions.TextInput;
import net.jimj.discord.agent.response.ActionRow;
import net.jimj.discord.agent.response.AutocompleteChoice;
import net.jimj.discord.agent.response.ButtonComponent;
import net.jimj.discord.agent.response.ImmutableAutocompleteChoice;
import net.jimj.discord.agent.response.ImmutableButtonComponent;
import net.jimj.discord.agent.response.ImmutableSelectMenuComponent;
import net.jimj.discord.agent.response.ImmutableTextInputComponent;
import net.jimj.discord.agent.response.SelectMenuComponent;
import net.jimj.discord.agent.response.SelectOption;
import net.jimj.discord.agent.response.StatefulComponent;
import net.jimj.discord.agent.response.TextInputComponent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BidInteractionHandler implements CommandInteraction {
    private static final String CANCEL_BID_INTERACTION_ID = "cancelBid";
    private static final String RAIDER_BID_INTERACTION_ID = "raiderBid";
    private static final String BOX_BID_INTERACTION_ID = "boxBid";
    private static final String BOX_SELECT_INTERACTION_ID = "boxPick";
    private static final String BID_AMOUNT_MODAL = "bidAmount";
    private static final String BID_INPUT = "bidInput";
    private static final String STATE_BIDDING_CHARACTER = "c";
    private static final String STATE_AUCTION_ID = "i";
    private static final String STATE_BIDDER_RANK = "r";

    private final Auctioneer<BidContext> auctioneer;
    private final BidProcessor bidProcessor;
    private final CancelBidInteraction cancelInteraction = new CancelBidInteraction();
    private final StartRaiderBid startRaiderBid = new StartRaiderBid();
    private final StartBoxBidInteraction startBoxBidInteraction = new StartBoxBidInteraction();

    private final BoxSelectedInteraction boxSelectedInteraction = new BoxSelectedInteraction();
    private final BidAmountModalInteraction bidAmountModalInteraction = new BidAmountModalInteraction();

    BidInteractionHandler(
            final Auctioneer<BidContext> auctioneer,
            final BidProcessor bidProcessor) {
        this.auctioneer = auctioneer;
        this.bidProcessor = bidProcessor;
    }

    @Override
    public String id() {
        return "bid";
    }

    private static JsonObject createBidButtonState(final String auctionId, final GuildConstants.Rank rank) {
        return new JsonObject()
                .put(STATE_AUCTION_ID, auctionId)
                .put(STATE_BIDDER_RANK, rank.ordinal());
    }

    public static List<ActionRow> generateBidButtons(
            final String auctionId) {

        final ButtonComponent startRaiderBid = ImmutableButtonComponent.builder()
                .label("Bid as Raider")
                .id(BidInteractionHandler.RAIDER_BID_INTERACTION_ID)
                .style(ButtonComponent.Style.PRIMARY)
                .state(createBidButtonState(auctionId, GuildConstants.Rank.RAIDER))
                .build();

        final ButtonComponent startBoxBid = ImmutableButtonComponent.builder()
                .label("Bid as Box")
                .id(BidInteractionHandler.BOX_BID_INTERACTION_ID)
                .style(ButtonComponent.Style.PRIMARY)
                .state(createBidButtonState(auctionId, GuildConstants.Rank.BOX))
                .build();

        final ButtonComponent startAltBid = ImmutableButtonComponent.builder()
                .label("Bid as Alt")
                .id(BidInteractionHandler.RAIDER_BID_INTERACTION_ID)
                .style(ButtonComponent.Style.SECONDARY)
                .state(createBidButtonState(auctionId, GuildConstants.Rank.ALT))
                .build();

        return List.of(new ActionRow(startRaiderBid, startBoxBid, startAltBid));
    }

    @Override
    public void accept(
            final InteractionRegistry registry) {

        registry.registerCommand(this);
        cancelInteraction.accept(registry);
        startRaiderBid.accept(registry);
        startBoxBidInteraction.accept(registry);
        boxSelectedInteraction.accept(registry);
        bidAmountModalInteraction.accept(registry);
    }

    @Override
    public void onAutocomplete(
            final String name,
            final InteractionContext interactionContext) {

        final List<AutocompleteChoice> suggestions = switch (name) {
            case "item" -> listRunningAuctions();
            case "character" -> listRaiders(interactionContext.userContext().preferences());
            default -> Collections.emptyList();
        };

        interactionContext.interaction()
                .autocomplete(suggestions);
    }

    @Override
    public void onInvoke(
            final InteractionContext interactionContext) {

        final RaiderSettings raiderSettings = RaiderSettings.read(interactionContext.userContext().preferences());

        if (!interactionContext.options().hasOption("character")
                && raiderSettings.mainRaider().isEmpty()) {
            interactionContext.interaction().reply("You need to configure a main raider before you can bid.");
        }

        final AuctionParser.ParsedBid parsedBid = AuctionParser.parseBid(interactionContext);
        bidProcessor.handleBid(
                parsedBid,
                interactionContext);
    }

    private List<AutocompleteChoice> listRunningAuctions() {
        final List<RunningAuction<BidContext>> runningAuctions = auctioneer.runningAuctions();

        if (runningAuctions.isEmpty()) {
            return Collections.singletonList(
                    ImmutableAutocompleteChoice.builder().name("No Running Auctions Available").value("INVALID_AUCTION_WILL_FAIL").build());
        } else {
            return runningAuctions.stream()
                    .map(runningAuction -> ImmutableAutocompleteChoice.builder().name(runningAuction.getName()).value(runningAuction.getName()).build())
                    .collect(Collectors.toList());
        }
    }

    private List<AutocompleteChoice> listRaiders(
            final UserPreferences userPreferences) {

        final RaiderSettings raiderSettings = RaiderSettings.read(userPreferences);

        final List<String> raiders = new ArrayList<>();
        raiderSettings.mainRaider().ifPresent(raiders::add);
        raiders.addAll(raiderSettings.boxRaiders());

        return raiders.stream()
                .map(raider -> ImmutableAutocompleteChoice.builder().name(raider).value(raider).build())
                .collect(Collectors.toList());
    }

    public class CancelBidInteraction
            implements ComponentInteraction {
        @Override
        public void onInvoke(StatefulComponent component, InteractionContext interactionContext, JsonObject componentData) {
            final String itemId = component.state().getString(STATE_AUCTION_ID);
            final String biddingCharacter = component.state().getString(STATE_BIDDING_CHARACTER);

            final InteractionFacade toUpdate = interactionContext.parentInteraction()
                    .orElse(interactionContext.interaction());

            interactionContext.interaction()
                    .acknowledge()
                    .compose(ignored -> auctioneer.cancelBid(itemId, interactionContext.userContext().snowflake(), biddingCharacter))
                    .onSuccess(ignored -> toUpdate.update(String.format("Your bid for \"%s\" has been canceled.", itemId)))
                    .onFailure(err -> toUpdate.update("I couldn't cancel the bid.  The auction is over or the bid was already cancelled."));
        }

        @Override
        public String id() {
            return CANCEL_BID_INTERACTION_ID;
        }

        @Override
        public void accept(
                final InteractionRegistry registry) {
            registry.registerComponent(this);
        }

        public static ButtonComponent generateCancelButton(
                final String auctionId,
                final String bidderName) {
            final JsonObject bidState = new JsonObject()
                    .put(STATE_AUCTION_ID, auctionId)
                    .put(STATE_BIDDING_CHARACTER, bidderName);

            return ImmutableButtonComponent.builder()
                    .label("Cancel Bid")
                    .style(ButtonComponent.Style.DANGER)
                    .id(CANCEL_BID_INTERACTION_ID)
                    .state(bidState)
                    .build();
        }
    }

    private static class StartRaiderBid implements ComponentInteraction {
        @Override
        public String id() {
            return RAIDER_BID_INTERACTION_ID;
        }

        @Override
        public void onInvoke(
                final StatefulComponent component,
                final InteractionContext interactionContext, JsonObject componentData) {
            final Optional<String> mainRaider = RaiderSettings
                    .read(interactionContext.userContext().preferences())
                    .mainRaider();

            if (mainRaider.isEmpty()) {
                interactionContext.interaction()
                        .reply("You must claim raid characters first.  Use the Raid Settings application by right-clicking on your name.");
                return;
            }

            final String biddingCharacter = mainRaider.get();
            final JsonObject state = component.state()
                .put(STATE_BIDDING_CHARACTER, biddingCharacter);

            final int bidderRankOrdinal = component.state().getInteger(STATE_BIDDER_RANK);

            final String bidLabel;
            if (bidderRankOrdinal == GuildConstants.Rank.RAIDER.ordinal()) {
                bidLabel = "Bid for " + biddingCharacter;
            } else {
                bidLabel = "Bid as ALT of " + biddingCharacter;
            }

            final TextInputComponent bidInput = ImmutableTextInputComponent.builder()
                    .id(BID_INPUT)
                    .style(TextInputComponent.Style.SHORT)
                    .label(bidLabel)
                    .required(true)
                    .build();

            interactionContext.interaction()
                    .modal(BID_AMOUNT_MODAL, "Enter bid", state, bidInput);
        }


        @Override
        public void accept(
                final InteractionRegistry registry) {
            registry.registerComponent(this);
        }
    }

    private static class StartBoxBidInteraction implements ComponentInteraction {
        @Override
        public void onInvoke(
                final StatefulComponent component,
                final InteractionContext interactionContext, JsonObject componentData) {
            final List<String> boxRaiders = RaiderSettings
                    .read(interactionContext.userContext().preferences())
                    .boxRaiders();

            if (boxRaiders.isEmpty()) {
                interactionContext.interaction()
                        .reply("You must claim boxes first.  Use the Raid Settings application by right-clicking on your name.");
                return;
            }

            final List<SelectOption> boxOptions = boxRaiders.stream()
                    .map(boxName -> new SelectOption(boxName, boxName, false))
                    .toList();
            final SelectMenuComponent boxMenu = ImmutableSelectMenuComponent.builder()
                    .id(BOX_SELECT_INTERACTION_ID)
                    .state(component.state())
                    .options(boxOptions)
                    .build();

            interactionContext.interaction()
                    .reply("Choose a box", List.of(new ActionRow(boxMenu)));
        }

        @Override
        public String id() {
            return BOX_BID_INTERACTION_ID;
        }

        @Override
        public void accept(
                final InteractionRegistry registry) {
            registry.registerComponent(this);
        }
    }

    private static class BoxSelectedInteraction implements ComponentInteraction {

        @Override
        public void onInvoke(
                final StatefulComponent component,
                final InteractionContext interactionContext,
                final JsonObject componentData) {

            final String selectedBox = componentData
                    .getJsonArray("values")
                    .getString(0);

            final JsonObject state = component.state()
                    .put(STATE_BIDDING_CHARACTER, selectedBox);

            final TextInputComponent bidInput = ImmutableTextInputComponent.builder()
                    .id(BID_INPUT)
                    .style(TextInputComponent.Style.SHORT)
                    .label("Bid for " + selectedBox)
                    .required(true)
                    .build();

            interactionContext.interaction()
                    .modal(BID_AMOUNT_MODAL, "Enter bid", state, bidInput);
        }

        @Override
        public String id() {
            return BOX_SELECT_INTERACTION_ID;
        }

        @Override
        public void accept(
                final InteractionRegistry registry) {
            registry.registerComponent(this);
        }
    }

    private class BidAmountModalInteraction implements ModalInteraction {

        @Override
        public String id() {
            return BID_AMOUNT_MODAL;
        }

        @Override
        public void accept(
                final InteractionRegistry registry) {
            registry.registerModal(this);
        }

        @Override
        public void onInvoke(
                final List<TextInput> modalInputs,
                final StatefulComponent component,
                final InteractionContext interactionContext) {

            if (modalInputs.isEmpty()) {
                interactionContext.interaction()
                        .reply("Please enter an amount.  Sometimes the discord client is buggy, it helps to delete and re-type the amount with each bid.");
                return;
            }

            final String bidderSnowflake = interactionContext.userContext().snowflake();
            final int amount;
            try {
                amount = Integer.parseInt(modalInputs.get(0).value());
            } catch (final NumberFormatException e) {
                interactionContext.interaction()
                        .reply("You must enter a number for your bid");
                return;
            }

            final String auctionId = component.state().getString(STATE_AUCTION_ID);
            final String itemName = auctioneer.lookupAuctionName(auctionId).orElse("unknown");

            final String character = component.state().getString(STATE_BIDDING_CHARACTER);
            final GuildConstants.Rank rank = GuildConstants.Rank.values()[component.state().getInteger(STATE_BIDDER_RANK)];


            final AuctionParser.ParsedBid parsedBid = new AuctionParser.ParsedBid(
                    itemName,
                    new Bid<>(bidderSnowflake, character, amount, new BidContext(rank)));

            bidProcessor.handleBid(
                    parsedBid,
                    interactionContext);
        }
    }
}
