package guilds.disciples;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import net.jimj.bidbud.auction.Auction;
import net.jimj.bidbud.auction.Auctioneer;
import net.jimj.bidbud.auction.Bid;
import net.jimj.bidbud.auction.DuplicateAuctionException;
import net.jimj.bidbud.auction.Loot;
import net.jimj.bidbud.auction.RunningAuction;
import net.jimj.bidbud.dkp.DKPAgent;
import net.jimj.bidbud.dkp.eqdkp.Raider;
import net.jimj.bidbud.lootview.EmbedImageProvider;
import net.jimj.bidbud.lootview.EmbeddableLoot;
import net.jimj.discord.agent.DiscordAgent;
import net.jimj.discord.agent.MessageContext;
import net.jimj.discord.agent.MessageFacade;
import net.jimj.discord.agent.MessageSelector;
import net.jimj.discord.agent.MessageSelectors;
import net.jimj.discord.agent.TextChannelFacade;
import net.jimj.discord.agent.UserPreferenceStorage;
import net.jimj.discord.agent.interactions.InteractionRegistry;
import net.jimj.discord.agent.response.Attachment;
import net.jimj.discord.agent.response.Embed;
import net.jimj.discord.agent.response.ImmutableEmbed;
import net.jimj.discord.agent.response.ImmutableTextMessage;
import net.jimj.discord.agent.response.TextMessage;
import net.jimj.discord.vertx.HttpFailure;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public class DisciplesAuctionAgent {
    private final Logger log = LogManager.getLogger(this.getClass());

    //Auction controls
    private static final MessageSelector START_AUCTION = MessageSelectors.toGuildId(GuildConstants.SERVER)
            .and(MessageSelectors.toChannelId(GuildConstants.AUCTION_CHANNEL))
            .and(MessageSelectors.contentMatches(AuctionParser::looksLikeAuction));
    private static final MessageSelector CANCEL_AUCTION = MessageSelectors.toGuildId(GuildConstants.SERVER)
            .and(MessageSelectors.toChannelId(GuildConstants.AUCTION_CHANNEL))
            .and(MessageSelectors.contentMatches(AuctionParser::looksLikeAuctionCancel));
    private static final MessageSelector PLACE_BID = MessageSelectors.isPrivate()
            .and(MessageSelectors.contentMatches(AuctionParser::looksLikeBid));
    private static final MessageSelector PLACE_SIMPLIFIED_BID = MessageSelectors.isPrivate()
            .and(MessageSelectors.contentMatches(AuctionParser::looksLikeSimplifiedBid));
    private static final MessageSelector HELP_BID = MessageSelectors.isPrivate()
            .and(MessageSelectors.contentMatches(AuctionParser::looksLikeHelpIsNeeded));

    //Administrative commands
    private static final MessageSelector REVEAL_AUCTION = MessageSelectors.toGuildId(GuildConstants.SERVER)
            .and(MessageSelectors.toChannelId(GuildConstants.CONTROL_CHANNEL))
            .and(MessageSelectors.contentMatches(AuctionParser::looksLikeAuctionReveal));
    private static final MessageSelector AMEND_AUCTION = MessageSelectors.toGuildId(GuildConstants.SERVER)
            .and(MessageSelectors.toChannelId(GuildConstants.CONTROL_CHANNEL))
            .and(MessageSelectors.contentMatches(AuctionParser::looksLikeAuctionAmend));

    private final Vertx vertx;
    private final Auctioneer<BidContext> auctioneer;
    private final DKPAgent dkpAgent;
    private final BidProcessor bidProcessor;
    private final Map<String, SettledAuction> finishedAuctions = new HashMap<>();

    private final BotSettings botSettings = BotSettings.instance();
    private final EmbedImageProvider embedImageProvider;

    public DisciplesAuctionAgent(
            final DiscordAgent agent,
            final DKPAgent dkpAgent,
            final EmbedImageProvider embedImageProvider) {

        this.vertx = agent.getVertx();
        this.auctioneer = new Auctioneer<>(vertx);
        this.dkpAgent = dkpAgent;
        this.embedImageProvider = embedImageProvider;
        this.bidProcessor = new BidProcessor(this.dkpAgent, this.auctioneer);

        agent.onMessage(START_AUCTION, this::startAuction);
        agent.onMessage(PLACE_BID, (messageContext) -> tryBid(messageContext, AuctionParser::parseBid));
        agent.onMessage(PLACE_SIMPLIFIED_BID, (messageContext) -> tryBid(messageContext, AuctionParser::parseSimplifiedBid));
        agent.onMessage(REVEAL_AUCTION, this::revealAuction);
        agent.onMessage(CANCEL_AUCTION, this::cancelAuction);
        agent.onMessage(HELP_BID, this::helpBid);
        agent.onMessage(AMEND_AUCTION, this::amendAuction);

        InteractionRegistry interactionRegistry = InteractionRegistry.instance();

        new BidInteractionHandler(this.auctioneer, this.bidProcessor).accept(interactionRegistry);
        new ClaimRaiderInteractionHandler(UserPreferenceStorage.getInstance()).accept(interactionRegistry);
    }


    private void startAuction(
            final MessageContext messageContext) {

        final AuctionParser.ParsedAuction parsedAuction ;
        try {
            parsedAuction = AuctionParser.parseStart(messageContext.message());
        } catch (final AuctionParser.ParseException e) {
            log.warn("Message content failed to parse as auction.", e);
            messageContext.channel().send("Sorry, something went wrong and I can't start the auction.");
            return;
        }

        final Duration minimumBidDuration =
                parsedAuction.duration() == Duration.ZERO
                ? Duration.ofMinutes(botSettings.get().getInteger("auctionLength"))
                : parsedAuction.duration();

        try {
            final Loot loot = new Loot(parsedAuction.name(), parsedAuction.quantity());
            final RunningAuction<BidContext> runningAuction = auctioneer.startAuction(loot, minimumBidDuration);
            final String content = MessageTemplates.runningAuctionMessage(runningAuction, parsedAuction.quantity());

            final Embed auctionEmbed = ImmutableEmbed.builder()
                    .title(String.format("%s", runningAuction.getName()))
                    .footer("Auction ID: " + runningAuction.getId())
                    .build();


            final TextMessage textMessage = ImmutableTextMessage.builder()
                    .content(content)
                    .embed(auctionEmbed)
                    .components(BidInteractionHandler.generateBidButtons(runningAuction.getId()))
                    .build();

            final Future<Optional<EmbeddableLoot>> lootEmbed = embedImageProvider.provide(parsedAuction.name());
            final ImmutableEmbed.Builder prettyEmbedBuilder = ImmutableEmbed.builder().from(auctionEmbed);
            final List<Attachment> attachments = new ArrayList<>();

            final Future<MessageFacade> messageFuture = messageContext.channel().send(textMessage)
                    .onSuccess(handle -> bark(handle, parsedAuction.quantity(), runningAuction))
                    .compose(handle -> lootEmbed.compose(lootOptional -> {
                        lootOptional.ifPresent(embeddableLoot -> {
                            final Embed prettyEmbed = prettyEmbedBuilder
                                    .thumbnail("attachment://" + embeddableLoot.icon().fileName())
                                    .image("attachment://" + embeddableLoot.description().fileName())
                                    .build();
                            attachments.add(embeddableLoot.icon());
                            attachments.add(embeddableLoot.description());

                            handle.update(prettyEmbed, attachments);
                        });

                        return Future.succeededFuture(handle);
                    }));

            runningAuction.getAuction()
                    .onSuccess((auction) -> {
                        messageFuture.result().delete();
                        finishAuction(runningAuction.getId(), auction, prettyEmbedBuilder.build(), attachments, messageContext.channel(), !parsedAuction.isTest());
                    })
                    .onFailure(ignored -> {
                        messageFuture.result().delete();
                        messageContext.channel().send("Auction " + runningAuction.getId() + " of \"" + runningAuction.getName() + "\" was cancelled.");
                    });

        } catch (final DuplicateAuctionException e) {
            messageContext.channel().send("There is already an auction running with the same name.");
        }
    }

    private void finishAuction(
            final String auctionId,
            final Auction<BidContext> auction,
            final Embed auctionEmbed,
            final List<Attachment> embedAttachments,
            final TextChannelFacade channel,
            final boolean chargeDkp) {
        final DisciplesAuctionSettler auctionSettler = new DisciplesAuctionSettler();
        final SettledAuction settledAuction = auctionSettler.settle(auction);

        finishedAuctions.put(auctionId, settledAuction);
        displaySettledAuction(auctionId, auctionEmbed, embedAttachments, channel, settledAuction);

        if (settledAuction.settlements().isEmpty() || !chargeDkp) {
            return;
        }

        settledAuction.settlements()
                .forEach(winningBid -> chargeWinner(settledAuction.loot().name(), winningBid, channel));
    }

    private void displaySettledAuction(
            final String auctionId,
            final Embed embed,
            final List<Attachment> embedAttachments,
            final TextChannelFacade channel,
            final SettledAuction auction) {

        final boolean includeSummary = botSettings.get().getBoolean("auctionSummary", false);

        final Embed finishedAuctionEmbed = ImmutableEmbed.builder()
                .title(String.format("%s", auction.loot().name()))
                .description(MessageTemplates.completedAuctionBidders(auction, includeSummary))
                .footer("Auction ID: " + auctionId)
                .thumbnail(embed.thumbnail())
                .image(embed.image())
                .build();

        final String message = MessageTemplates.completedAuctionMessage(auction);

        final TextMessage completedAuctionMessage = ImmutableTextMessage.builder()
                .content(message)
                .embed(finishedAuctionEmbed)
                .attachments(embedAttachments)
                .build();
        channel.send(completedAuctionMessage);
    }

    private void chargeWinner(
            final String itemName,
            final Bid<BidContext> winningBid,
            final TextChannelFacade channel) {
        final Raider raider = dkpAgent.getRaider(winningBid.characterName()).get(); //Safe, character looked up during bidding.

        dkpAgent.chargeDKP(raider, itemName, winningBid.amount())
                .onSuccess(ignored -> log.info("Charged {} {} for {}", raider.getName(), winningBid.amount(), itemName))
                .onFailure(err -> {
                    log.error("Failed to charge {} {} for {}", raider.getName(), winningBid.amount(), itemName, err);
                    channel.send("Failed to charge " + raider.getName() + ": " + err.getMessage());
                });
    }

    private void cancelAuction(
            final MessageContext messageContext) {
        final String name = AuctionParser.parseCancelAuction(messageContext.message());
        auctioneer.cancelAuction(name);
    }

    private void revealAuction(
            final MessageContext messageContext) {

        final String auctionId = AuctionParser.parseReveal(messageContext.message());
        final SettledAuction auction = finishedAuctions.get(auctionId);

        if (auction == null) {
            messageContext.channel().send("I couldn't find an auction matching ID `" + auctionId + "`");
        } else {
            messageContext.channel().send(MessageTemplates.revealAuction(auctionId, auction));
        }
    }

    private void amendAuction(
            final MessageContext messageContext) {
        final AuctionParser.ParsedAmendment auctionAmendment = AuctionParser.parseAmend(messageContext.message());

        final SettledAuction settledAuction = finishedAuctions.get(auctionAmendment.auctionId());
        if (settledAuction == null) {
            messageContext.channel().send("I couldn't find an auction matching ID `" + auctionAmendment.auctionId() + "`");
            return;
        }

        final boolean hasBidder = settledAuction.bids().stream()
                .anyMatch(bid -> bid.characterName().equalsIgnoreCase(auctionAmendment.bidderSnowflake()));

        if (!hasBidder) {
            messageContext.channel().send("Auction `" + auctionAmendment.auctionId() + "` doesn't seem to have a bid for " + auctionAmendment.bidderSnowflake());
            return;
        }

        final List<Bid<BidContext>> amendedBids = settledAuction.bids()
                .stream()
                .filter(bid -> !(bid.characterName().equalsIgnoreCase(auctionAmendment.bidderSnowflake())))
                .toList();

        final Auction<BidContext> amendedAuction = new Auction<>(settledAuction.loot(), amendedBids);
        final DisciplesAuctionSettler auctionSettler = new DisciplesAuctionSettler();
        final SettledAuction reSettledAuction = auctionSettler.settle(amendedAuction);

        if(reSettledAuction.settlements().equals(settledAuction.settlements())) {
            messageContext.channel().send("Removing " + auctionAmendment.bidderSnowflake() + " from `" + auctionAmendment.auctionId() + " does not change the results.");
        } else {
            messageContext.channel().send(MessageTemplates.completedAuctionBidders(reSettledAuction, false));
            messageContext.channel().send("I can't update DKP yet.  A human will need to.");
        }
    }

    private void tryBid(
            final MessageContext messageContext,
            final Function<MessageContext, AuctionParser.ParsedBid> bidParser) {

        final AuctionParser.ParsedBid parsedBid = bidParser.apply(messageContext);
        bidProcessor.handleBid(
                parsedBid,
                messageContext);
    }

    private void helpBid(
            final MessageContext messageContext) {
        if (!auctioneer.runningAuctions().isEmpty()) {
            log.info("Sending bid help to {}", messageContext.userContext().name());
            messageContext.channel().send(MessageTemplates.helpBid());
        }
    }

    private void bark(
            final MessageFacade messageFacade,
            final int quantity,
            final RunningAuction<BidContext> auction) {

        final long nextUpdate = determineUpdateFrequency(auction);
        final String content = MessageTemplates.runningAuctionMessage(auction, quantity);

        vertx.setTimer(nextUpdate, (timerId) -> {
            messageFacade.update(content)
                    .onSuccess(ignored -> {
                        if (auction.getRemainingTime().toMillis() > 0) {
                            bark(messageFacade, quantity, auction);
                        }
                    })
                    .onFailure(err -> {
                        if (err instanceof final HttpFailure failure) {
                            if (failure.isNotFound()) {
                                auctioneer.cancelAuction(auction.getName());
                            }
                        }
                    });
        });
    }

    private long determineUpdateFrequency(
            final RunningAuction<BidContext> auction) {
        Duration updateFrequency = Duration.ofSeconds(30);

        final Duration remaining = auction.getRemainingTime();
        if (remaining.toMillis() <= Duration.ofSeconds(60).toMillis()) {
            updateFrequency = Duration.ofSeconds(5);
        }

        return updateFrequency.toMillis();
    }
}
