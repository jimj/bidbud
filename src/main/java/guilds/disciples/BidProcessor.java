package guilds.disciples;

import net.jimj.bidbud.auction.Auctioneer;
import net.jimj.bidbud.auction.Bid;
import net.jimj.bidbud.dkp.DKPAgent;
import net.jimj.bidbud.dkp.eqdkp.Raider;
import net.jimj.discord.agent.MessageContext;
import net.jimj.discord.agent.interactions.InteractionContext;
import net.jimj.discord.agent.response.ActionRow;
import net.jimj.discord.agent.response.ButtonComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Encapsulates logic to process a bid, ensuring the bidder has enough DKP.
 */
public class BidProcessor {
    record Disposition(boolean canAfford, int availableDKP) {
    }

    private final Logger logger = LogManager.getLogger(this.getClass());

    private final DKPAgent dkpAgent;
    private final Auctioneer<BidContext> auctioneer;

    BidProcessor(
            final DKPAgent dkpAgent,
            final Auctioneer<BidContext> auctioneer) {
        this.dkpAgent = dkpAgent;
        this.auctioneer = auctioneer;
    }

    public void handleBid(
            final AuctionParser.ParsedBid parsedBid,
            final InteractionContext interactionContext) {
        handleBid(
                parsedBid,
                message -> interactionContext.interaction().reply(message),
                (message, components) -> interactionContext.interaction().reply(message, components));
    }

    public void handleBid(
            final AuctionParser.ParsedBid parsedBid,
            final MessageContext messageContext) {
        handleBid(
                parsedBid,
                message -> messageContext.channel().send(message),
                (message, components) -> messageContext.channel().send(message, components));
    }

    private void handleBid(
            final AuctionParser.ParsedBid parsedBid,
            final Consumer<String> reply,
            final BiConsumer<String, List<ActionRow>> componentReply) {

        if (parsedBid.bid().amount() < 1) {
            reply.accept("You must bid at least 1 DKP");
            return;
        }
        final BidProcessor.Disposition disposition = canAfford(parsedBid);

        if (!disposition.canAfford()) {
            final String message = MessageTemplates.bidTooHigh(parsedBid.bid().characterName(), disposition.availableDKP());
            reply.accept(message);
        } else {
            final Optional<String> updatedAuctionId = auctioneer.hearBid(parsedBid.item(), parsedBid.bid());
            if (updatedAuctionId.isPresent()) {
                final String message = MessageTemplates.bidInteractionAccepted(parsedBid);
                final ButtonComponent cancelButton = BidInteractionHandler
                        .CancelBidInteraction
                        .generateCancelButton(updatedAuctionId.get(), parsedBid.bid().characterName());

                componentReply.accept(message, List.of(new ActionRow(cancelButton)));
            } else {
                reply.accept("Sorry, it looks like that auction has ended now.");
            }
        }
    }

    Disposition canAfford(
            final AuctionParser.ParsedBid parsedBid) {

        final Optional<Raider> raider = dkpAgent.getRaider(parsedBid.bid().characterName());
        if (raider.isEmpty()) {
            return new Disposition(false, 0);
        }

        final int outstandingBids = auctioneer.runningAuctions().stream()
                .filter(auction -> !auction.getName().equalsIgnoreCase(parsedBid.item()))
                .filter(auction -> auction.hasBidOn(parsedBid.bid().characterName()))
                .map(auction -> auction.getBid(parsedBid.bid().bidderSnowflake(), parsedBid.bid().characterName()))
                .filter(bid -> bid.characterName().equalsIgnoreCase(parsedBid.bid().characterName()))
                .mapToInt(BidProcessor::calculateMaxPrice)
                .sum();

        final int availableDKP = raider.get().getDkp();
        final int uncommittedDKP = availableDKP - outstandingBids;

        if (availableDKP <= 0 || uncommittedDKP < calculateMaxPrice(parsedBid.bid())) {
            logger.info("{} was denied a bid: outstanding {} | available {} | uncommitted {}",
                    parsedBid.bid().characterName(), outstandingBids, availableDKP, uncommittedDKP);
            return new Disposition(false, uncommittedDKP);
        }

        return new Disposition(true, uncommittedDKP);
    }

    static int calculateMaxPrice(final Bid<BidContext> bid) {
        return bid.amount() * bid.context().bidderRank().getMultiplier();
    }

    static Bid<BidContext> calculateFinalPrice(
            final Bid<BidContext> winningBid,
            final Bid<BidContext> priceSettingBid) {
        int basePrice;

        if (winningBid.context().bidderRank() != priceSettingBid.context().bidderRank()) {
             basePrice = Math.min(priceSettingBid.context().bidderRank().getCap(), priceSettingBid.amount()) + 1;
         } else {
             basePrice = priceSettingBid.amount() + 1;
         }
        basePrice = Math.min(winningBid.amount(), basePrice);

        final int price = winningBid.context().bidderRank().getMultiplier() * basePrice;

        return new Bid<>(
                winningBid.bidderSnowflake(),
                winningBid.characterName(),
                price,
                winningBid.context());
    }
}
