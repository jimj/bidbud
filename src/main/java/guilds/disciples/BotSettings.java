package guilds.disciples;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BotSettings {
    private static final String STORAGE_DIR = System.getenv("USER_PREFS_DIR");
    private static final String SETTINGS_FILE = "bot.settings";
    private final JsonObject settings;

    private static BotSettings INSTANCE;

    public static synchronized BotSettings instance() {
        if (INSTANCE == null) {
            INSTANCE = new BotSettings();
        }

        return INSTANCE;
    }

    private BotSettings() {
        if (STORAGE_DIR != null) {
            settings = load();
        } else {
            settings = new JsonObject();
        }
    }

    private void save() {
        final Path persistedPreferences = Paths.get(STORAGE_DIR, SETTINGS_FILE);
        try {
            Files.writeString(persistedPreferences, settings.toString());
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private JsonObject load() {
        final Path settingsFile = Paths.get(STORAGE_DIR, SETTINGS_FILE);
        final byte[] bytes;
        try {
            bytes = Files.readAllBytes(settingsFile);
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }

        final Buffer buffer = Buffer.buffer(bytes);
        return new JsonObject(buffer);
    }

    /**
     * Returns a read-only copy of the settings.
     * Modifying the returned value does not affect the bot settings.
     */
    public JsonObject get() {
        return settings.copy();
    }

    public void set(final String preferenceName, final Object preferenceValue) {
        settings.put(preferenceName, preferenceValue);
        save();
    }
}
