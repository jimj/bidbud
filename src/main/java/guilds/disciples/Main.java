package guilds.disciples;

import io.vertx.core.Vertx;
import net.jimj.bidbud.dkp.DKPAgent;
import net.jimj.bidbud.dkp.eqdkp.EqDKPVerticle;
import net.jimj.bidbud.lootview.PlaywrightVerticle;
import net.jimj.discord.agent.MessageSelectors;
import net.jimj.discord.agent.interactions.InteractionRegistry;
import net.jimj.discord.vertx.DiscordVerticle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;

public class Main {
    private static final Logger LOG = LogManager.getLogger(Main.class);

    public static void main(final String[] args) throws MalformedURLException {
        final String discordAuthToken = System.getenv("DISCORD_TOKEN");
        final DiscordVerticle discordVerticle = new DiscordVerticle(discordAuthToken);

        final URL dkpEndpoint = new URL(System.getenv("DKP_SITE"));
        final String dkpAuthToken = System.getenv("DKP_TOKEN");

        final EqDKPVerticle eqDKPVerticle = new EqDKPVerticle(dkpEndpoint, dkpAuthToken);
        final Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(discordVerticle);
        vertx.deployVerticle(eqDKPVerticle);

        final PlaywrightVerticle playwrightVerticle = new PlaywrightVerticle();
        vertx.deployVerticle(playwrightVerticle);

        final InteractionRegistry interactionRegistry = InteractionRegistry.instance();

        discordVerticle.agent()
                .onSuccess(discordAgent -> {
                    final DKPAgent dkpAgent = eqDKPVerticle.enableAgent(discordAgent);
                    discordAgent.onMessage(MessageSelectors.isPrivate(), (messageContext) -> {
                        LOG.info("Private Message from {} - {}", messageContext.userContext().name(), messageContext.message());
                    });
                    new AdminInteractionHandler().accept(interactionRegistry);
                    new DisciplesAuctionAgent(discordAgent, dkpAgent, playwrightVerticle);
                    new DKPDumpInteractionHandler(dkpAgent).accept(interactionRegistry);
                });
    }
}
