package guilds.disciples;

import net.jimj.discord.agent.interactions.CommandInteraction;
import net.jimj.discord.agent.interactions.InteractionContext;
import net.jimj.discord.agent.interactions.InteractionOptions;
import net.jimj.discord.agent.interactions.InteractionRegistry;
import net.jimj.discord.agent.response.AutocompleteChoice;
import net.jimj.discord.agent.response.ImmutableAutocompleteChoice;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AdminInteractionHandler implements CommandInteraction {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private static final List<Setting<?>> SUPPORTED_SETTINGS = Arrays.asList(
            new Setting<>("auctionLength", Integer::parseInt, "Duration of each auction, in minutes.", "number"),
            new Setting<>("auctionGap", Integer::parseInt, "Minimum gap applied between auction completions, in seconds.", "number"),
            new Setting<>("cats", Boolean::parseBoolean, "Cats.", "boolean"),
            new Setting<>("auctionSummary", Boolean::parseBoolean, "Include auction summary in result.", "boolean")
    );
    private final BotSettings botSettings = BotSettings.instance();

    @Override
    public String id() {
        return "admin";
    }

    @Override
    public void accept(
            final InteractionRegistry registry) {
        registry.registerCommand(this);
    }

    @Override
    public void onAutocomplete(String name, final InteractionContext interactionContext) {
        final List<AutocompleteChoice> choices = SUPPORTED_SETTINGS.stream()
                .map(setting -> ImmutableAutocompleteChoice.builder().name(setting.name()).value(setting.name()).build())
                .collect(Collectors.toList());

        interactionContext.interaction().autocomplete(choices);
    }

    @Override
    public void onInvoke(
            final InteractionContext interactionContext) {
        if (!interactionContext.options().isSubCommand()) {
            interactionContext.interaction().reply("You haven't included all the input needed for this command.  Please try again.");
        }

        switch (interactionContext.options().getSubCommandName()) {
            case "show":
                final StringBuilder settingsDisplay = new StringBuilder();
                settingsDisplay
                        .append(String.format("```%s```", botSettings.get().encodePrettily()))
                        .append("\n\n");
                SUPPORTED_SETTINGS.forEach(setting -> settingsDisplay.append(String.format("`%s` : %s\n", setting.name(), setting.description())));
                interactionContext.interaction().reply(settingsDisplay.toString());
                break;
            case "set":
                final InteractionOptions setOptions = interactionContext.options().getSubCommandOptions();
                final String settingName = setOptions.getStringValue("name");

                SUPPORTED_SETTINGS.stream()
                        .filter(s -> s.name.equalsIgnoreCase(settingName))
                        .findFirst()
                        .ifPresentOrElse(
                                setting -> setSetting(setting, interactionContext),
                                () -> interactionContext.interaction().reply(String.format("Sorry `%s` is not a setting name I understand.", settingName)));
                break;
            default:
                logger.info("Unhandled sub-command " + interactionContext.options().getSubCommandName());
        }
    }

    private void setSetting(
            final Setting<?> setting, InteractionContext interactionContext) {
        try {
            final Object newVal = setting.parse(interactionContext);
            botSettings.set(setting.name(), newVal);

            interactionContext.interaction().reply(String.format("`%s` set to %s", setting.name, newVal));
        } catch (IOException e) {
            logger.warn("");
        }
    }

    record Setting<T>(
            String name,
            Function<String, T> argumentTransform,
            String description,
            String humanReadableType) {

        T parse(final InteractionContext interactionContext) throws IOException {
            final String argumentValue = interactionContext.options()
                    .getSubCommandOptions()
                    .getStringValue("value");
            try {
                return argumentTransform.apply(argumentValue);
            } catch (final Exception e) {
                interactionContext.interaction().reply(String.format("Setting `%s` requires a %s value", name, humanReadableType));
                throw new IOException(e);
            }
        }
    }
}
