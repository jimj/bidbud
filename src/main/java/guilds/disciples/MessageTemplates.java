package guilds.disciples;

import net.jimj.bidbud.auction.Bid;
import net.jimj.bidbud.auction.RunningAuction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.Set;
import java.util.stream.Collectors;

public class MessageTemplates {
    public static String runningAuctionMessage(
            final RunningAuction<?> auction,
            final int quantity) {
        return String.format(
                "**[%dx %s]** Bids are open for %s",
                quantity, auction.getName(), formatDuration(auction.getRemainingTime()));
    }

    public static String completedAuctionMessage(
            final SettledAuction auction) {

        String mentions = "";
        if (!auction.settlements().isEmpty()) {
            final Set<String> uniqueWinnerSnowflakes = auction.settlements()
                    .stream()
                    .map(Bid::bidderSnowflake)
                    .collect(Collectors.toSet());

            mentions = uniqueWinnerSnowflakes.stream()
                    .map(snowflake -> String.format("<@%s>", snowflake))
                    .collect(Collectors.joining(" "));
        }

        return String.format("**[%dx %s]** Winners: %s", auction.loot().quantity(), auction.loot().name(), mentions);
    }

    public static String completedAuctionBidders(
            final SettledAuction auction,
            final boolean includeSummary) {

        if (auction.settlements().isEmpty()) {
            return "```No bids received.```";
        }

        final String winners = auction.settlements()
                .stream()
                .map(MessageTemplates::resultsFormat)
                .collect(Collectors.joining("\n"));

        String message = "```" + winners + "```";
        if (includeSummary) {
            final String summary = auctionSummary(auction);
            message += "\n```" + summary + "```";
        }

        return message;
    }

    public static String auctionSummary(
            final SettledAuction auction) {
        final StringBuilder summary = new StringBuilder();

        summary.append(String.format("There were %d bids.\nThe price-setting bid was %d DKP.", auction.bids().size(), auction.priceSetter().amount()));

        final long tiedCount = auction.bids().stream().filter(bid -> bid.amount() == auction.priceSetter().amount()).count();
        if (tiedCount > 1) {
            summary.append(String.format("\nThere was a roll-off between %d bidders.", tiedCount));
        }

        return summary.toString();
    }

    public static String revealAuction(
            final String auctionId,
            final SettledAuction auction) {

        String winnerMessage  = "";
        String bidsMessage  = "No bids received.";
        if (!auction.bids().isEmpty()) {
            winnerMessage = auction.settlements().stream()
                    .map(bid -> String.format("%s for %d", bid.characterName(), bid.amount()))
                    .collect(Collectors.joining(", "));
            bidsMessage = auction.bids().stream()
                    .map(MessageTemplates::revealFormat)
                    .collect(Collectors.joining("\n"));
        }

        return loadTemplate("reveal_auction")
                .replaceAll("%quantity%", Integer.toString(auction.loot().quantity()))
                .replaceAll("%name%", auction.loot().name())
                .replaceAll("%winners%", winnerMessage)
                .replaceAll("%bids%", bidsMessage)
                .replaceAll("%auctionid%", auctionId);
    }

    public static String bidInteractionAccepted(
            final AuctionParser.ParsedBid bid) {

        return loadTemplate("bid_interaction_accepted")
                .replaceAll("%name%", bid.item())
                .replaceAll("%character%", bid.bid().characterName())
                .replaceAll("%maxprice%", Integer.toString(BidProcessor.calculateMaxPrice(bid.bid())));
    }

    public static String bidTooHigh(
            final String character,
            final int availableDKP) {
        return String.format("%s only has %d left to bid with.  Cancel some bids or bid less.", character, availableDKP);
    }

    public static String helpBid() {
        return loadTemplate("help_bid");
    }

    private static String formatDuration(final Duration duration) {
        if (duration.isNegative() || duration.isZero()) {
            return "";
        }

        return duration.toMinutes() + "min "
                + ((duration.toMillis() / 1000) % 60) + "sec";
    }


    private static String loadTemplate(
            final String templateName) {

        try (final InputStream inputStream = MessageTemplates.class.getClassLoader().getResourceAsStream("message_templates/" + templateName);
             final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            return reader.lines().collect(Collectors.joining("\n"));
        } catch (final IOException e) {
            return "ERROR";
        }
    }

    private static String resultsFormat(
            final Bid<BidContext> bid) {
        return String.format("%s as %s for %d", bid.characterName(), bid.context().bidderRank().name(), bid.amount());
    }

    private static String revealFormat(final Bid<BidContext> bid) {
        return String.format("{%s: %d} (%s)", bid.characterName(), bid.amount(), bid.context().bidderRank().toString());
    }
}
