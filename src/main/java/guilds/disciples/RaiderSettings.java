package guilds.disciples;

import net.jimj.discord.agent.UserPreferences;

import java.util.List;
import java.util.Optional;

public record RaiderSettings(
        Optional<String> mainRaider,
        List<String> boxRaiders) {

    static final String RAIDER = "mainRaider";
    static final String BOX = "boxRaiders";

    public static RaiderSettings read(
            final UserPreferences preferences) {
        final String mainRaider = preferences.getString(RAIDER);
        final List<String> boxes = preferences.getStrings(BOX);

        return new RaiderSettings(
                Optional.ofNullable(mainRaider),
                boxes);
    }
}
