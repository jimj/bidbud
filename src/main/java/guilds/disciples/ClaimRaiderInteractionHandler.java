package guilds.disciples;

import io.vertx.core.json.JsonObject;
import net.jimj.discord.agent.UserPreferenceStorage;
import net.jimj.discord.agent.interactions.CommandInteraction;
import net.jimj.discord.agent.interactions.InteractionContext;
import net.jimj.discord.agent.interactions.InteractionRegistry;
import net.jimj.discord.agent.interactions.ModalInteraction;
import net.jimj.discord.agent.interactions.TextInput;
import net.jimj.discord.agent.response.ImmutableTextInputComponent;
import net.jimj.discord.agent.response.StatefulComponent;
import net.jimj.discord.agent.response.TextInputComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class ClaimRaiderInteractionHandler implements CommandInteraction {
    private static final String CLAIM_MODAL = "claimRaider";
    private static final String RAIDER_INPUT = "m";
    private static final String BOX_INPUT = "b";

    private final Logger logger = LogManager.getLogger(this.getClass());
    private final UserPreferenceStorage userPreferenceStorage;

    public ClaimRaiderInteractionHandler(
            final UserPreferenceStorage userPreferenceStorage) {
        this.userPreferenceStorage = userPreferenceStorage;
    }

    @Override
    public String id() {
        return "Raider Settings";
    }

    @Override
    public void accept(
            final InteractionRegistry registry) {
        registry.registerCommand(this);
        new RaiderModal().accept(registry);
    }

    @Override
    public void onAutocomplete(
            final String name,
            final InteractionContext interactionContext) {

    }

    @Override
    public void onInvoke(
            final InteractionContext interactionContext) {

        final RaiderSettings raiderSettings = RaiderSettings.read(
                interactionContext.userContext().preferences());

        final TextInputComponent main = ImmutableTextInputComponent.builder()
                .id(RAIDER_INPUT)
                .label("Main Raider")
                .value(raiderSettings.mainRaider().orElse(""))
                .required(true)
                .style(TextInputComponent.Style.SHORT)
                .build();

        final TextInputComponent boxes = ImmutableTextInputComponent.builder()
                .id(BOX_INPUT)
                .label("Boxes (comma separated)")
                .required(false)
                .value(String.join(",", raiderSettings.boxRaiders()))
                .style(TextInputComponent.Style.SHORT)
                .build();

        interactionContext.interaction().modal(
                CLAIM_MODAL,
                "Enter the characters you bid with below.",
                new JsonObject(),
                main, boxes);
    }

    private class RaiderModal implements ModalInteraction {
        @Override
        public String id() {
            return CLAIM_MODAL;
        }

        @Override
        public void accept(
                final InteractionRegistry registry) {
            registry.registerModal(this);
        }

        @Override
        public void onInvoke(
                final List<TextInput> modalInputs,
                final StatefulComponent component,
                final InteractionContext interactionContext) {
            final String userId = interactionContext.userContext().snowflake();
            for (TextInput input : modalInputs) {
                switch (input.name()) {
                    case RAIDER_INPUT -> {
                        userPreferenceStorage.savePreference(userId, RaiderSettings.RAIDER, input.value());
                        logger.info("{} saved mainRaider [{}]", interactionContext.userContext().name(), input.value());
                        interactionContext.interaction().reply("Raider Settings Saved.");
                    }
                    case BOX_INPUT -> {
                        final List<String> boxRaiders = Arrays.stream(input.value().split(","))
                                .map(String::trim)
                                .toList();

                        userPreferenceStorage.savePreference(userId, RaiderSettings.BOX, boxRaiders);
                        logger.info("{} saved boxRaiders [{}]", interactionContext.userContext().name(), input.value());
                    }
                    default -> logger.info("Unsupported input: {}", input);
                }
            }
        }
    }
}
