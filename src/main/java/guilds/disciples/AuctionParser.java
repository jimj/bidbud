package guilds.disciples;

import net.jimj.bidbud.auction.Bid;
import net.jimj.discord.agent.MessageContext;
import net.jimj.discord.agent.interactions.InteractionContext;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuctionParser {
    private static final Pattern START = Pattern.compile("^.dkp (?<mode>start|test)bids\\s+((?<preQuantity>[0-9]{0,3})x?\\s+)?[\"\\p{Pi}\\p{Pf}](?<itemName>.+)[\"\\p{Pi}\\p{Pf}](\\s+(?<duration>[0-9]))?");
    private static final Pattern CANCEL = Pattern.compile("^.dkp cancel\\s+[\"\\p{Pi}\\p{Pf}](?<itemName>.+)[\"\\p{Pi}\\p{Pf}]");
    private static final Pattern REVEAL = Pattern.compile("^.dkp reveal\\s+(?<auctionId>\\w+)");
    private static final Pattern AMEND = Pattern.compile("^.dkp amend\\s+(?<auctionId>\\w+)\\s+(?<bidder>\\w+)");
    private static final Pattern PLACE_BID = Pattern.compile("[\"\\p{Pi}\\p{Pf}]?(?<itemName>[^\"\\p{Pi}\\p{Pf}]+)[\"\\p{Pi}\\p{Pf}]?\\s+(?<character>\\w+)\\s+(?<bid>[0-9]+)\\s+(?<rank>\\w+)$");
    private static final Pattern PLACE_SIMPLIFIED_BID = Pattern.compile("[\"\\p{Pi}\\p{Pf}]?(?<itemName>[^\"\\p{Pi}\\p{Pf}]+)[\"\\p{Pi}\\p{Pf}]?\\s+(?<bid>[0-9]+)$");

    static boolean looksLikeAuction(final String content) {
        return START.matcher(content).matches();
    }

    static boolean looksLikeAuctionCancel(final String content) {
        return CANCEL.matcher(content).matches();
    }

    static boolean looksLikeAuctionReveal(final String content) {
        return REVEAL.matcher(content).matches();
    }

    static boolean looksLikeAuctionAmend(final String content) {
        return AMEND.matcher(content).matches();
    }

    static boolean looksLikeSimplifiedBid(final String content) {
        return PLACE_SIMPLIFIED_BID.matcher(content).matches();
    }

    static boolean looksLikeBid(final String content) {
        return PLACE_BID.matcher(content).matches();
    }

    static boolean looksLikeBidCancel(final String content) {
        return content.startsWith("cancel")
                || content.endsWith("cancel");
    }

    static boolean looksLikeHelpIsNeeded(final String content) {
        final String normalizedContent = content.toLowerCase();
        return !looksLikeBid(normalizedContent)
                && !looksLikeSimplifiedBid(normalizedContent)
                && !looksLikeBidCancel(normalizedContent)
                && !normalizedContent.startsWith(".dkp")
                && !normalizedContent.startsWith("dkp")
                && !normalizedContent.endsWith("dkp");
    }

    static ParsedAuction parseStart(
            final String content) throws ParseException {

        final Matcher matcher = START.matcher(content);
        if (matcher.matches()) {
            final boolean isTest = matcher.group("mode").equals("test");
            final String itemName = matcher.group("itemName")
                    .trim()
                    .replaceAll("\"", "");

            int quantity = 1;
            if (matcher.group("preQuantity") != null) {
                quantity = Integer.parseInt(matcher.group("preQuantity"));
            }

            Duration duration = Duration.ZERO;
            if (matcher.group("duration") != null) {
                duration = Duration.ofMinutes(Integer.parseInt(matcher.group("duration")));
            }

            return new ParsedAuction(itemName, quantity, duration, isTest);
        }

        throw new ParseException("Pattern did not match");
    }

    static ParsedBid parseBid(
            final MessageContext messageContext) throws ParseException {

        final Matcher matcher = PLACE_BID.matcher(messageContext.message());

        if (matcher.matches()) {
            final String item = matcher.group("itemName").trim();
            final String character = matcher.group("character").trim();
            final int amount = Integer.parseInt(matcher.group("bid"));
            final GuildConstants.Rank rank = GuildConstants.Rank.from(matcher.group("rank").trim())
                    .orElseThrow(() -> new ParseException("Invalid Rank: " + matcher.group("rank")));

            final BidContext bidContext = new BidContext(rank);
            final Bid<BidContext> bid = new Bid<>(messageContext.userContext().snowflake(), character, amount, bidContext);
            return new ParsedBid(item, bid);
        }

        throw new ParseException("Pattern did not match");
    }

    static ParsedBid parseSimplifiedBid(
            final MessageContext messageContext)  {

        final Matcher matcher = PLACE_SIMPLIFIED_BID.matcher(messageContext.message());

        if (matcher.matches()) {
            final RaiderSettings raiderSettings = RaiderSettings.read(messageContext.userContext().preferences());

            final String character = raiderSettings.mainRaider().orElse("UNKNOWN");
            final String item = matcher.group("itemName").trim();
            final int amount = Integer.parseInt(matcher.group("bid"));

            final BidContext bidContext = new BidContext(GuildConstants.Rank.RAIDER);
            final Bid<BidContext> bid = new Bid<>(messageContext.userContext().snowflake(), character, amount, bidContext);
            return new ParsedBid(item, bid);
        }

        throw new ParseException("Pattern did not match");
    }

    static ParsedBid parseBid(
            final InteractionContext interactionContext) {

        final RaiderSettings raiderSettings = RaiderSettings.read(interactionContext.userContext().preferences());
        final String mainRaider = raiderSettings.mainRaider().orElse("UNKNOWN");

        final String biddingCharacter = interactionContext.options().hasOption("character")
                ? interactionContext.options().getStringValue("character")
                : mainRaider;

        final GuildConstants.Rank characterRank;
        if (raiderSettings.boxRaiders().contains(biddingCharacter)) {
            characterRank = GuildConstants.Rank.BOX;
        } else {
            characterRank = GuildConstants.Rank.RAIDER;
        }

        final String item = interactionContext.options().getStringValue("item");
        final int amount = interactionContext.options().getIntValue("amount");

        final BidContext bidContext = new BidContext(characterRank);
        final Bid<BidContext> bid = new Bid<>(interactionContext.userContext().snowflake(), biddingCharacter, amount, bidContext);
        return new ParsedBid(item, bid);
    }

    static String parseCancelAuction(
            final String message) {
        final Matcher matcher = CANCEL.matcher(message);

        if(matcher.matches()) {
            return matcher.group("itemName").trim();
        }

        throw new IllegalArgumentException(message + " did not match expected format.");
    }

    static String parseReveal(
            final String message) {
        final Matcher matcher = REVEAL.matcher(message);

        if (matcher.matches()) {
            return matcher.group("auctionId");
        }

        throw new IllegalArgumentException("Invalid message passed");
    }

    static ParsedAmendment parseAmend(
            final String message) {
        final Matcher matcher = AMEND.matcher(message);

        if (matcher.matches()) {
            final String auctionId = matcher.group("auctionId");
            final String bidder = matcher.group("bidder");

            return new ParsedAmendment(auctionId, bidder);
        }

        throw new IllegalArgumentException("Invalid message passed");
    }

    static class ParseException extends RuntimeException {
        ParseException(final String reason) {
            super(reason);
        }
    }

    record ParsedAuction(String name, int quantity, Duration duration, boolean isTest) { }

    record ParsedBid(String item, Bid<BidContext> bid) { }

    record ParsedAmendment(String auctionId, String bidderSnowflake) { }
}
