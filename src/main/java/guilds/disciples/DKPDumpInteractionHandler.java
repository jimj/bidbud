package guilds.disciples;

import io.vertx.core.json.JsonObject;
import net.jimj.bidbud.dkp.DKPAgent;
import net.jimj.bidbud.dkp.eqdkp.Raider;
import net.jimj.discord.agent.interactions.CommandInteraction;
import net.jimj.discord.agent.interactions.InteractionContext;
import net.jimj.discord.agent.interactions.InteractionRegistry;
import net.jimj.discord.agent.interactions.ModalInteraction;
import net.jimj.discord.agent.interactions.TextInput;
import net.jimj.discord.agent.response.ImmutableTextInputComponent;
import net.jimj.discord.agent.response.StatefulComponent;
import net.jimj.discord.agent.response.TextInputComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DKPDumpInteractionHandler implements CommandInteraction {
    private static final String DESCRIPTION_INPUT = "n";
    private static final String AMOUNT_INPUT = "a";
    private static final String DUMP_INPUT = "d";
    private static final String STANDBY_INPUT = "s";
    private static final String SUBMIT_DUMP_MODAL = "submitDump";

    private final DKPAgent dkpAgent;

    public DKPDumpInteractionHandler(
            final DKPAgent dkpAgent) {
        this.dkpAgent = dkpAgent;
    }

    @Override
    public void onInvoke(
            final InteractionContext interactionContext) {
        final TextInputComponent descriptionInput = ImmutableTextInputComponent.builder()
                .id(DESCRIPTION_INPUT)
                .label("Description")
                .style(TextInputComponent.Style.SHORT)
                .state(new JsonObject())
                .required(true)
                .build();

        final TextInputComponent amountInput = ImmutableTextInputComponent.builder()
                .id(AMOUNT_INPUT)
                .label("DKP Amount")
                .style(TextInputComponent.Style.SHORT)
                .state(new JsonObject())
                .required(true)
                .build();

        final TextInputComponent dumpInput = ImmutableTextInputComponent.builder()
                .id(DUMP_INPUT)
                .label("RaidRoster input")
                .style(TextInputComponent.Style.PARAGRAPH)
                .state(new JsonObject())
                .required(true)
                .build();

        final TextInputComponent standbyInput = ImmutableTextInputComponent.builder()
                .id(STANDBY_INPUT)
                .label("Standby List (1 name per line)")
                .style(TextInputComponent.Style.PARAGRAPH)
                .state(new JsonObject())
                .required(false)
                .build();

        interactionContext.interaction()
                .modal(SUBMIT_DUMP_MODAL, "Award DKP to Raid", new JsonObject(), descriptionInput, amountInput, dumpInput, standbyInput);
    }

    @Override
    public void onAutocomplete(
            final String name,
            final InteractionContext interactionContext) {

    }

    @Override
    public String id() {
        return "raiddump";
    }

    @Override
    public void accept(
            final InteractionRegistry registry) {
        registry.registerCommand(this);
        new SubmitDumpModal().accept(registry);
    }

    private class SubmitDumpModal implements ModalInteraction {
        private final Logger logger = LogManager.getLogger(this.getClass());
        @Override
        public String id() {
            return SUBMIT_DUMP_MODAL;
        }

        @Override
        public void accept(
                final InteractionRegistry registry) {
            registry.registerModal(this);
        }

        @Override
        public void onInvoke(
                final List<TextInput> modalInputs,
                final StatefulComponent component,
                final InteractionContext interactionContext) {
            final String description = modalInputs.get(0).value();

            final int amount;
            try {
                amount = Integer.parseInt(modalInputs.get(1).value());
            } catch (final NumberFormatException e){
                interactionContext.interaction().reply("The amount must be a whole number.  Try again.");
                return;
            }

            final Set<String> raidMembers = new HashSet<>();
            raidMembers.addAll(extractRaidersFromDump(modalInputs.get(2).value()));
            raidMembers.addAll(extractRaiderNamesFromStandby(modalInputs.get(3).value()));

            final List<Raider> raiders = raidMembers.stream()
                    .flatMap(raiderName -> dkpAgent.getRaider(raiderName).stream())
                    .toList();

            if (raiders.size() != raidMembers.size()) {
                final StringBuilder failureReason = new StringBuilder()
                        .append("FAILED!  I cannot create new raiders on the website.  This dump will need to be uploaded on the site.\n")
                        .append("New Raiders:\n")
                        .append("```\n");

                raiders.stream()
                        .map(Raider::getName)
                        .forEach(raidMembers::remove);
                raidMembers.forEach(member -> failureReason.append(member).append("\n"));
                failureReason.append("```");

                interactionContext.interaction().reply(failureReason.toString());
                return;
            }

            //TODO: Adding a member to EQDKP
            // https://github.com/EQdkpPlus/core/blob/bdb195ccca779fa80fe0410ecd2f450a96e11ff5/core/pageobjects/addcharacter_pageobject.class.php
            // https://github.com/EQdkpPlus/plugin-raidlogimport

            dkpAgent.payDKP(description, amount, raiders)
                    .onSuccess((ignored) -> {
                        final String success = String.format("Created raid [%s] worth %d DKP and %d members.", description, amount, raiders.size());
                        interactionContext.interaction().reply(success);
                    })
                    .onFailure((err) -> {
                        logger.warn("Failed to award DKP", err);
                        interactionContext.interaction().reply("Sorry, something went wrong.  You'll have to try again.");
                    });
        }

        private List<String> extractRaidersFromDump(final String raidRoster) {
            if (raidRoster == null || raidRoster.isBlank()) {
                return Collections.emptyList();
            }

            return Arrays.stream(raidRoster.split("\n"))
                    .map(rosterRow -> rosterRow.split("\t"))
                    .map(rosterRow -> rosterRow[1].trim())
                    .toList();
        }

        private List<String> extractRaiderNamesFromStandby(final String standby) {
            if (standby == null || standby.isBlank()) {
                return Collections.emptyList();
            }

            return Arrays.stream(standby.split("\n"))
                    .map(String::trim)
                    .toList();
        }
    }
}
