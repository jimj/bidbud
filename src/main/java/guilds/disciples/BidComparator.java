package guilds.disciples;

import net.jimj.bidbud.auction.Bid;

import java.util.Comparator;

public class BidComparator implements Comparator<Bid<BidContext>> {
    @Override
    public int compare(
            final Bid<BidContext> bid1,
            final Bid<BidContext> bid2) {
        final int bid1Val = getAdjustedBidAmount(bid1, bid2);
        final int bid2Val = getAdjustedBidAmount(bid2, bid1);

        return Integer.compare(bid1Val, bid2Val) * -1;
    }

    private int getAdjustedBidAmount(
            final Bid<BidContext> bidToAdjust,
            final Bid<BidContext> bidToCompare) {
        if (bidToAdjust.context().bidderRank() != bidToCompare.context().bidderRank()) {
            return Math.min(bidToAdjust.amount(), bidToAdjust.context().bidderRank().getCap());
        } else {
            return bidToAdjust.amount();
        }
    }
}
