package net.jimj.bidbud.dkp.eqdkp;

import io.vertx.core.json.JsonObject;

import java.util.Objects;

public class Raider {
    private final String id;
    private final String name;
    private final String className;
    private final int dkp;
    private boolean isActive;

    public Raider(String id, String name, String className, int dkp, boolean isActive) {
        this.id = id;
        this.name = name;
        this.className = className;
        this.dkp = dkp;
        this.isActive = isActive;
    }

    Raider(JsonObject player) {
        final String points = player.getJsonObject("points").getJsonObject("multidkp_points:1").getString("points_current_with_twink");
        this.id = player.getString("main_id");
        this.name = player.getString("name");
        this.className = player.getString("class_name").replaceAll(" ", "");
        this.dkp = (int) Double.parseDouble(points);
        this.isActive = player.getString("active").equals("1");
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getClassName() {
        return className;
    }

    public int getDkp() {
        return dkp;
    }

    public boolean isActive() {
        return isActive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Raider raider = (Raider) o;
        return dkp == raider.dkp && Objects.equals(id, raider.id) && Objects.equals(name, raider.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, dkp);
    }
}
