package net.jimj.bidbud.dkp.eqdkp;

import io.vertx.core.json.JsonObject;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Roster {
    private final List<Raider> raiders = new ArrayList<>();
    private final Instant whenRefreshed = Instant.now();

    Roster(JsonObject players) {
        players.stream()
                .map(entry -> players.getJsonObject(entry.getKey()))
                .map(Raider::new)
                .forEach(raiders::add);
    }

    public Instant getWhenRefreshed() {
        return whenRefreshed;
    }

    public Optional<Raider> findRaider(final String name) {
        return raiders.stream()
                .filter(raider -> raider.getName().equalsIgnoreCase(name))
                .findFirst();
    }

    public Stream<Raider> findRaidersByClass(final String className) {
        return raiders.stream()
                .filter(raider -> raider.getClassName().equalsIgnoreCase(className));
    }

    /**
     * Adjusts the cached DKP value without requiring a refresh from the site.
     */
    public synchronized void adjustDKP(
            final Raider raider,
            final int adjustment) {

        if (raiders.remove(raider)) {
            final int newDKP = raider.getDkp() + adjustment;
            final Raider adjustedRaider = new Raider(raider.getId(), raider.getName(), raider.getClassName(), newDKP, raider.isActive());
            raiders.add(adjustedRaider);
        }
    }
}
