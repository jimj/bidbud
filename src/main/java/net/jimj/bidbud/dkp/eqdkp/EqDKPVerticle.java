package net.jimj.bidbud.dkp.eqdkp;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import net.jimj.bidbud.dkp.DKPAgent;
import net.jimj.bidbud.lootview.EmbedImageProvider;
import net.jimj.discord.agent.DiscordAgent;

import java.net.URL;
import java.time.Duration;

public class EqDKPVerticle extends AbstractVerticle {
    private final URL endpoint;
    private final String authToken;

    private DKPAgent agent;
    private DKPWebClient dkpClient;
    private Roster cachedRoster;
    private Raid cachedRaid;

    public EqDKPVerticle(
            final URL endpoint,
            final String authToken) {
        this.endpoint = endpoint;
        this.authToken = authToken;
    }

    @Override
    public void init(
            final Vertx vertx,
            final Context context) {
        super.init(vertx, context);

        this.dkpClient = new DKPWebClient(vertx, endpoint, authToken);
        vertx.setPeriodic(Duration.ofMinutes(15).toMillis(), (ignored) -> {
            refreshRaid();
            refreshRoster();
        });
    }

    public synchronized DKPAgent enableAgent(
            final DiscordAgent discordAgent) {
        if (agent == null) {
            agent = new DKPAgent(discordAgent, dkpClient, () -> cachedRoster, () -> cachedRaid);
        }

        return agent;
    }

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        refreshRoster()
                .compose(ignored -> refreshRaid())
                .onSuccess(ignored -> startPromise.complete())
                .onFailure(startPromise::fail);
    }

    private Future<Roster> refreshRoster() {
        return dkpClient.getRoster()
                .onSuccess(roster -> this.cachedRoster = roster);
    }

    private Future<Raid> refreshRaid() {
        return dkpClient.getLatestRaid()
                .onSuccess(raid -> this.cachedRaid = raid);
    }
}
