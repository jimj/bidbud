package net.jimj.bidbud.dkp.eqdkp;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

public class DKPWebClient {
    private static final String API = "api.php";
    private static final Supplier<String> TIMESTAMP = () -> DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
            .format(LocalDateTime.now(ZoneId.of("America/New_York"))); //TODO: Make this settings based.

    private final Logger log = LogManager.getLogger(this.getClass());

    private final WebClient webClient;
    private final String authToken;
    private final String apiPath;

    public DKPWebClient(
            final Vertx vertx,
            final URL endpoint,
            final String authToken) {

        final WebClientOptions options = new WebClientOptions()
                .setDefaultHost(endpoint.getHost());

        if (endpoint.getProtocol().equalsIgnoreCase("https")) {
            options.setDefaultPort(443);
            options.setSsl(true);
        } else {
            options.setDefaultPort(80);
            options.setSsl(false);
        }

        this.webClient = WebClient.create(vertx, options);
        this.apiPath = endpoint.getPath().endsWith("/")
                ? endpoint.getPath() + API
                : endpoint.getPath() + "/" + API;

        this.authToken = authToken;
    }

    public Future<Raid> getLatestRaid() {
        log.info("Fetching latest raid from DKP site.");
        final Promise<Raid> raidPromise = Promise.promise();
        final Instant startLookup = Instant.now();
        webClient.get(apiPath)
                .putHeader("Authorization", authToken)
                .setQueryParam("format", "json")
                .setQueryParam("function", "raids")
                .setQueryParam("number", "1")
                .send()
                .onSuccess(response -> {
                    log.info("Raid Lookup took {} ms", Duration.between(startLookup, Instant.now()).toMillis());
                    final JsonObject body = response.bodyAsJsonObject();

                    final Optional<String> raidKey = body.stream()
                            .filter(entry -> entry.getKey().startsWith("raid"))
                            .map(Map.Entry::getKey)
                            .findFirst();

                    if (raidKey.isPresent()) {
                        final JsonObject raidBody = body.getJsonObject(raidKey.get());
                        raidPromise.complete(new Raid(
                                Integer.parseInt(raidBody.getString("id")),
                                Instant.ofEpochSecond(Long.parseLong(raidBody.getString("date_timestamp"))),
                                raidBody.getString("note")));
                    } else {
                        raidPromise.fail("No raids found.");
                    }
                })
                .onFailure(err -> {
                    log.info("Roster Fetch Failed after {} ms", Duration.between(startLookup, Instant.now()).toMillis());
                    raidPromise.fail(err);
                });

        return raidPromise.future();
    }

    public Future<Void> chargeDKP(
            final Raid raid,
            final Raider raider,
            final String item,
            final int amount) {

        final JsonObject request = new JsonObject()
                .put("item_date", TIMESTAMP.get())
                .put("item_name", item)
                .put("item_value", Integer.toString(amount))
                .put("item_buyers", new JsonObject().put("member", new JsonArray().add(raider.getId())))
                .put("item_raid_id", Integer.toString(raid.getId()))
                .put("item_itempool_id", "1");

        final Promise<Void> charged = Promise.promise();

        webClient.post(apiPath)
                .putHeader("Authorization", authToken)
                .expect(ResponsePredicate.SC_SUCCESS)
                .setQueryParam("format", "json")
                .setQueryParam("function", "add_item")
                .sendBuffer(Buffer.buffer(request.encode()))
                .onComplete(result -> {
                    if (result.failed()) {
                        charged.fail(result.cause());
                    } else if (result.result().statusCode() >= 400) {
                        charged.fail("Status Code " + result.result().statusCode());
                    } else {
                        charged.complete();
                    }
                });

        return charged.future();
    }

    public Future<Void> payDKP(
            final String description,
            final int amount,
            final List<String> memberIds) {

        final JsonArray raid_attendees = new JsonArray();
        memberIds.forEach(raid_attendees::add);

        final JsonObject request = new JsonObject()
                .put("raid_date", TIMESTAMP.get())
                .put("raid_event_id", "1")
                .put("raid_note", description)
                .put("raid_value", Integer.toString(amount))
                .put("raid_attendees", new JsonObject().put("member", raid_attendees));

        log.debug("POST add_raid\n{}", request.encodePrettily());

        final Promise<Void> paid = Promise.promise();

        webClient.post(apiPath)
                .putHeader("Authorization", authToken)
                .expect(ResponsePredicate.SC_SUCCESS)
                .setQueryParam("format", "json")
                .setQueryParam("function", "add_raid")
                .sendBuffer(Buffer.buffer(request.encode()))
                .onComplete(result -> {
                    if (result.failed()) {
                        paid.fail(result.cause());
                    } else if (result.result().statusCode() >= 400) {
                        paid.fail("Status Code " + result.result().statusCode());
                    } else {
                        log.debug("RESPONSE:\n{}", result.result().bodyAsString());
                        paid.complete();
                    }
                });

        return paid.future();
    }

    public Future<Roster> getRoster() {
        final Promise<Roster> rosterPromise = Promise.promise();

        final Instant startLookup = Instant.now();
        webClient.get(apiPath)
                .putHeader("Authorization", authToken)
                .setQueryParam("format", "json")
                .setQueryParam("function", "points")
                .send()
                .onSuccess(response -> {
                    log.info("Roster Fetch Success in {} ms", Duration.between(startLookup, Instant.now()).toMillis());
                    rosterPromise.complete(new Roster(response.bodyAsJsonObject().getJsonObject("players")));
                })
                .onFailure(err -> {
                    log.info("Roster Fetch Failed after {} ms", Duration.between(startLookup, Instant.now()).toMillis());
                    rosterPromise.fail(err);
                });

        return rosterPromise.future();
    }
}
