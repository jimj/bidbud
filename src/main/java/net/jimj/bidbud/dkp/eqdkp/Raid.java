package net.jimj.bidbud.dkp.eqdkp;

import java.time.Instant;

public class Raid {
    private final int id;
    private final Instant startTime;
    private final String note;

    public Raid(
            final int id,
            final Instant startTime,
            final String note) {
        this.id = id;
        this.startTime = startTime;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public String getNote() {
        return note;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Raid{");
        sb.append("id=").append(id);
        sb.append(", startTime=").append(startTime);
        sb.append(", note='").append(note).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
