package net.jimj.bidbud.dkp;

import guilds.disciples.BotSettings;
import guilds.disciples.RaiderSettings;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import net.jimj.bidbud.cats.CatWebClient;
import net.jimj.bidbud.dkp.eqdkp.DKPWebClient;
import net.jimj.bidbud.dkp.eqdkp.Raid;
import net.jimj.bidbud.dkp.eqdkp.Raider;
import net.jimj.bidbud.dkp.eqdkp.Roster;
import net.jimj.discord.agent.DiscordAgent;
import net.jimj.discord.agent.MessageContext;
import net.jimj.discord.agent.MessageSelector;
import net.jimj.discord.agent.MessageSelectors;
import net.jimj.discord.agent.Snowflakes;
import net.jimj.discord.agent.TextChannelFacade;
import net.jimj.discord.agent.response.Attachment;
import net.jimj.discord.agent.response.Embed;
import net.jimj.discord.agent.response.ImmutableAttachment;
import net.jimj.discord.agent.response.ImmutableEmbed;
import net.jimj.discord.agent.response.ImmutableTextMessage;
import net.jimj.discord.agent.response.TextMessage;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class DKPAgent {
    private static final MessageSelector CHECK_DKP = MessageSelectors.contentMatches(InputParser::looksLikeDKPCheck);
    private static final Set<String> EQ_CLASSES = new HashSet<>(
            Arrays.asList(
                    "bard", "beastlord", "berserker", "cleric", "druid", "enchanter", "magician", "monk",
                    "necromancer", "paladin", "ranger", "rogue", "shadowknight", "shaman", "warrior", "wizard"
                    ));
    private final BotSettings botSettings = BotSettings.instance();

    private final DKPWebClient dkpClient;
    private final Supplier<Roster> rosterSupplier;
    private final Supplier<Raid> raidSupplier;

    private CatWebClient catWebClient;

    public DKPAgent(
            final DiscordAgent agent,
            final DKPWebClient dkpClient,
            final Supplier<Roster> rosterSupplier,
            final Supplier<Raid> raidSupplier) {
        this.dkpClient = dkpClient;
        this.rosterSupplier = rosterSupplier;
        this.raidSupplier = raidSupplier;

        agent.onMessage(CHECK_DKP, this::reportDKP);

        final String catApiToken = System.getenv("CAT_API_TOKEN");
        if (catApiToken != null) {
            this.catWebClient = new CatWebClient(agent.getVertx(), catApiToken);
        }
    }

    public Optional<Raider> getRaider(final String characterName) {
        return rosterSupplier.get().findRaider(characterName);
    }

    public Raid getRaid() {
        return raidSupplier.get();
    }

    public Future<Void> chargeDKP(final Raider winner, final String itemName, final int cost) {
        final Raid raid = getRaid();

        return dkpClient.chargeDKP(raid, winner, itemName, cost)
                .onSuccess(ignored -> rosterSupplier.get().adjustDKP(winner, cost * -1));
    }

    public Future<Void> payDKP(final String reason, final int amount, final List<Raider> members) {
        final List<String> memberIds = members.stream()
                .map(Raider::getId)
                .toList();

        return dkpClient.payDKP(reason, amount, memberIds);
    }

    private void reportDKP(
            final MessageContext messageContext) {

        final Roster roster = rosterSupplier.get();
        final String characterName = InputParser.parseCharacterName(messageContext.message())
                .orElse(RaiderSettings.read(messageContext.userContext().preferences()).mainRaider().orElse("UNKNOWN"));

        if (EQ_CLASSES.contains(characterName.toLowerCase())) {
            reportClassDKP(roster, characterName, messageContext.channel());
        } else {
            reportCharacterDKP(roster, characterName, messageContext.channel());
        }
    }

    private void reportClassDKP(
            final Roster roster,
            final String className,
            final TextChannelFacade channel) {

        final int limit = 10;
        final String header = String.format("Top %d %s DKP:\n", limit, className);
        final StringBuilder dkpMessage = new StringBuilder(header);
        dkpMessage.append("```\n");

        roster.findRaidersByClass(className)
                .filter(Raider::isActive)
                .sorted(Comparator.comparing(Raider::getDkp).reversed())
                .limit(limit)
                .forEach(raider -> dkpMessage.append(String.format("%-5d %s\n", raider.getDkp(), raider.getName())));

        dkpMessage.append("```\n");
        dkpMessage.append(String.format("I last checked the site %s ago.", getPrettyStaleness(roster)));

        channel.send(dkpMessage.toString());
    }

    private void reportCharacterDKP(
            final Roster roster,
            final String characterName,
            final TextChannelFacade channel) {
        final String dkpMessage = roster.findRaider(characterName)
                .map(raider -> String.format(
                        "I think %s has **%d** DKP available to spend.  I last checked the site %s ago.",
                        characterName,
                        raider.getDkp(),
                        getPrettyStaleness(roster)))
                .orElse("Didn't find a character named " + characterName);

        channel.send(dkpMessage);

        if (characterName.equalsIgnoreCase("magg")) {
            final Buffer maggImg = Buffer.buffer(readMagg());

            final Embed embed = ImmutableEmbed.builder()
                    .image("attachment://magg.jpg")
                    .build();
            final Attachment magg = ImmutableAttachment.builder()
                    .content(maggImg)
                    .name("magg.jpg")
                    .fileName("magg.jpg")
                    .id(Snowflakes.attachment())
                    .mediaType(Attachment.MediaType.JPG)
                    .build();
            final TextMessage message = ImmutableTextMessage.builder()
                    .embed(embed)
                    .addAttachments(magg)
                    .build();

            channel.send(message);
        } else if (catWebClient != null && botSettings.get().getBoolean("cats")) {
            catWebClient.getCatImageLocation()
                    .onSuccess(catImage -> {
                        final Embed embed = ImmutableEmbed.builder().image(catImage).build();
                        channel.send(embed);
                    });
        }
    }

    private String getPrettyStaleness(final Roster roster) {
        final StringBuilder prettyStaleness = new StringBuilder();
        final Duration staleness = Duration.between(roster.getWhenRefreshed(), Instant.now());

        if (staleness.getSeconds() >= 60) {
            prettyStaleness.append(staleness.getSeconds() / 60).append("m ");
        }

        prettyStaleness.append(staleness.getSeconds() % 60).append("s");

        return prettyStaleness.toString();
    }

    private byte[] readMagg() {
        try (InputStream magg = getClass().getClassLoader().getResourceAsStream("magg.jpg")) {
            final byte[] magBuffer = new byte[15_000];
            final int read = magg.read(magBuffer);

            return Arrays.copyOf(magBuffer, read);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
