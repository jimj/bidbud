package net.jimj.bidbud.dkp;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class InputParser {
    private static final Pattern CHECK_DKP = Pattern.compile("(^\\.?(dkp|DKP)\\s+(?<character>\\w*)|(?<character2>\\w*)\\s+\\.?(dkp|DKP)$)");

    static boolean looksLikeDKPCheck(
            final String message) {
        return message.equalsIgnoreCase(".dkp") || CHECK_DKP.matcher(message).matches();
    }

    static Optional<String> parseCharacterName(
            final String content) {
        if (content.equalsIgnoreCase(".dkp")) {
            return Optional.empty();
        }

        final Matcher matcher = CHECK_DKP.matcher(content);
        if (matcher.matches()) {
            final String inputName = matcher.group("character") != null
                    ? matcher.group("character")
                    : matcher.group("character2");

            return Optional.ofNullable(inputName);
        }

        throw new IllegalArgumentException(content + " does not match expected pattern.");
    }
}
