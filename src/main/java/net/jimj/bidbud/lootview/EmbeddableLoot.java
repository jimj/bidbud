package net.jimj.bidbud.lootview;

import net.jimj.discord.agent.response.Attachment;

public record EmbeddableLoot(Attachment icon, Attachment description) {

}
