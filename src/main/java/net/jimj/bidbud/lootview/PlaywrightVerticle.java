package net.jimj.bidbud.lootview;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.ElementHandle;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import com.microsoft.playwright.TimeoutError;
import com.microsoft.playwright.options.WaitForSelectorState;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import net.jimj.discord.agent.Snowflakes;
import net.jimj.discord.agent.response.Attachment;
import net.jimj.discord.agent.response.ImmutableAttachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class PlaywrightVerticle extends AbstractVerticle implements EmbedImageProvider {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private static final Page.WaitForSelectorOptions VISIBLE = new Page.WaitForSelectorOptions().setState(WaitForSelectorState.VISIBLE).setTimeout(7_000);
    private static final Page.WaitForSelectorOptions QUICKLY_VISIBLE = new Page.WaitForSelectorOptions().setState(WaitForSelectorState.VISIBLE).setTimeout(3_000);

    private Playwright playwright;
    private Browser browser;
    private BrowserContext browserContext;

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        this.playwright = Playwright.create();
    }

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        getVertx().executeBlocking(future -> {
            logger.info("Launching Chromium");
            browser = playwright.chromium().launch();
            browserContext = browser.newContext();
            logger.info("Chromium launched.");

            try(final Page page = browserContext.newPage()) {
                page.navigate("https://everquest.allakhazam.com");
                page.bringToFront();
                page.waitForTimeout(250);
                try {
                    page.waitForSelector("#onetrust-reject-all-handler", VISIBLE);
                    page.click("#onetrust-reject-all-handler");
                    logger.info("Cookies Rejected");
                } catch (final TimeoutError e) {
                    logger.info("No cookie banner appeared");
                }

                future.complete(browser);
                startPromise.complete();
            }
        }, res -> {
            if (res.failed()) {
                logger.info("Verticle startup failed", res.cause());
            }
        });
    }

    @Override
    public void stop(Promise<Void> stopPromise) throws Exception {
        getVertx().executeBlocking(future -> {
            browser.close();
            playwright.close();
        }, res -> stopPromise.complete());
    }

    @Override
    public Future<Optional<EmbeddableLoot>> provide(String item) {
        final Promise<Optional<EmbeddableLoot>> promise = Promise.promise();

        vertx.<Optional<EmbeddableLoot>>executeBlocking(future -> {
            try(final Page page = browserContext.newPage()) {
                page.navigate("https://everquest.allakhazam.com");
                page.bringToFront();
                page.type("#header > form[name='search'] > input[name='q']", item);
                page.waitForSelector("#livesearch > ul > li:nth-child(1) > a", QUICKLY_VISIBLE);
                page.hover("#livesearch > ul > li:nth-child(1) > a");
                page.waitForTimeout(250); //ZAM lazy-loads the image on hover
                page.waitForSelector("#headtt > img", VISIBLE);

                final ElementHandle toolTip = page.querySelector("#headtt");
                final byte[] icon = toolTip.querySelector("img").screenshot();
                final byte[] description = toolTip.screenshot();

                final Attachment iconAttachment = createIconAttachment(icon);
                final Attachment descriptionAttachment = createDescriptionAttachment(description);
                future.complete(Optional.of(new EmbeddableLoot(iconAttachment, descriptionAttachment)));
            } catch (final Exception e){
                future.complete(Optional.empty());
            }
        }, res -> {
            if (res.failed()) {
                promise.complete(Optional.empty());
            } else {
                promise.complete(res.result());
            }
        });

        return promise.future();
    }

    private Attachment createIconAttachment(
            final byte[] imageBytes) {

        return ImmutableAttachment.builder()
                .content(Buffer.buffer(imageBytes))
                .name("icon.png")
                .fileName("icon.png")
                .id(Snowflakes.attachment())
                .mediaType(Attachment.MediaType.PNG)
                .build();
    }

    private Attachment createDescriptionAttachment(
            final byte[] imageBytes) {

        return ImmutableAttachment.builder()
                .content(Buffer.buffer(imageBytes))
                .name("description.png")
                .fileName("description.png")
                .id(Snowflakes.attachment())
                .mediaType(Attachment.MediaType.PNG)
                .build();
    }
}
