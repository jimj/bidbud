package net.jimj.bidbud.lootview;

import io.vertx.core.Future;

import java.util.Optional;

@FunctionalInterface
public interface EmbedImageProvider {
    Future<Optional<EmbeddableLoot>> provide(
            final String id);
}
