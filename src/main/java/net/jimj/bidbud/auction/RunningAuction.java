package net.jimj.bidbud.auction;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class RunningAuction<CTX> {
    private final Logger logger = LogManager.getLogger(this.getClass());

    private final String id;
    private final String name;
    private final Instant startTime;
    private final Duration duration;
    private final Promise<Auction<CTX>> auctionPromise;
    private final HashMap<String, Map<String, Bid<CTX>>> bids = new HashMap<>();

    RunningAuction(
            final String name,
            final Instant startTime,
            final Duration duration,
            final Promise<Auction<CTX>> auctionPromise) {
        this.id = UUID.randomUUID().toString().substring(0, 6);
        this.name = name;
        this.startTime = startTime;
        this.duration = duration;
        this.auctionPromise = auctionPromise;

        this.auctionPromise
                .future()
                .onSuccess(ignored -> auctionLog("complete"));

        auctionLog("created: " + name);
    }

    private void auctionLog(
            final String action) {
        logger.info("{}: {}", id, action);
    }

    private void bidLog(
            final Bid<CTX> bid,
            final String action) {
        logger.info("{}: bid {} - {}", id, bid, action);
    }

    void acceptBid(
            final Bid<CTX> bid) {
        bids.compute(bid.bidderSnowflake(),
                (bidderSnowFlake, existingBids) -> {
                    final Map<String, Bid<CTX>> updatedBids =
                            existingBids == null
                            ? new HashMap<>()
                            : existingBids;

                    if (updatedBids.containsKey(bid.characterName())) {
                        bidLog(updatedBids.get(bid.characterName()), "replaced");
                    }
                    bidLog(bid, "created");
                    updatedBids.put(bid.characterName(), bid);
                    return updatedBids;
        });
    }

    Bid<CTX> removeBid(
            final String bidderSnowflake,
            final String characterName) {
        final Map<String, Bid<CTX>> playerBids = bids.get(bidderSnowflake);
        final Bid<CTX> bid = playerBids.remove(characterName);
        if (bid != null) {
            bidLog(bid, "removed");
        }

        return bid;
    }

    public Bid<CTX> getBid(
            final String bidderSnowflake,
            final String characterName) {
        return bids.get(bidderSnowflake).get(characterName);
    }

    public boolean hasBidOn(final String bidderSnowflake) {
        return bids.containsKey(bidderSnowflake);
    }

    void cancel() {
        if (!auctionPromise.future().isComplete()) {
            auctionLog("cancelled");
            auctionPromise.fail("Cancelled");
        }
    }

    List<Bid<CTX>> getBids() {
        return bids.values().stream()
                .flatMap(playerBids -> playerBids.values().stream())
                .toList();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Future<Auction<CTX>> getAuction() {
        return auctionPromise.future();
    }

    public Instant getStartTime() {
        return startTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public Duration getRemainingTime() {
        return getDuration()
                .minus(Duration.between(getStartTime(), Instant.now()));
    }
}
