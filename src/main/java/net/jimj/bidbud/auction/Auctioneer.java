package net.jimj.bidbud.auction;

import guilds.disciples.BotSettings;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class Auctioneer<CTX> {
    private final Vertx vertx;
    private final Map<RunningAuction<CTX>, Long> runningAuctions = new HashMap<>();
    private final BotSettings botSettings = BotSettings.instance();
    private Instant latestEndTime = Instant.now();

    public Auctioneer(
            final Vertx vertx) {
        this.vertx = vertx;
    }

    public RunningAuction<CTX> startAuction(
            final Loot loot,
            final Duration minimumBiddingTime) throws DuplicateAuctionException {

        final Duration auctionDuration;
        final Promise<Auction<CTX>> auctionPromise = Promise.promise();
        final RunningAuction<CTX> runningAuction;


        synchronized (runningAuctions) {
            if (findAuctionEntry(loot.name()).isPresent()) {
                throw new DuplicateAuctionException();
            }

            auctionDuration = getNextAuctionDuration(minimumBiddingTime);

            runningAuction = new RunningAuction<>(
                    loot.name(),
                    Instant.now(),
                    auctionDuration,
                    auctionPromise);

            final long timerId = vertx.setTimer(auctionDuration.toMillis(), (ignored) -> {
                synchronized (runningAuctions) {
                    runningAuctions.remove(runningAuction);
                }

                auctionPromise.complete(new Auction<>(loot, runningAuction.getBids()));
            });

            runningAuctions.put(runningAuction, timerId);
        }

        return runningAuction;
    }

    public boolean cancelAuction(
            final String name) {

        final AtomicBoolean canceled = new AtomicBoolean(false);
        synchronized (runningAuctions) {
            findAuctionEntry(name)
                    .ifPresent(entry -> {
                        final RunningAuction<CTX> runningAuction = entry.getKey();
                        synchronized (runningAuctions) {
                            runningAuctions.remove(runningAuction);
                        }
                        vertx.cancelTimer(entry.getValue());
                        runningAuction.cancel();
                        canceled.set(true);
                    });
        }

        return canceled.get();
    }

    public Optional<String> hearBid(
            final String item,
            final Bid<CTX> bid) {
        final Optional<RunningAuction<CTX>> auction = findAuctionEntry(item)
                .map(Map.Entry::getKey);

        auction.ifPresent(a -> a.acceptBid(bid));

        return auction.map(RunningAuction::getId);
    }

    public Future<Bid<CTX>> cancelBid(
            final String item,
            final String bidderSnowflake,
            final String characterName) {
        final Promise<Bid<CTX>> bidPromise = Promise.promise();
        final Optional<RunningAuction<CTX>> auction = findAuctionEntry(item)
                .map(Map.Entry::getKey);

        if(auction.isEmpty()) {
            bidPromise.fail("No Auction");
        } else {
            final Bid<CTX> previousBid = auction.get().removeBid(bidderSnowflake, characterName);
            if (previousBid == null) {
                bidPromise.fail("No Bid");
            } else {
                bidPromise.complete(previousBid);
            }
        }

        return bidPromise.future();
    }

    private Duration getNextAuctionDuration(
            final Duration requestedDuration) {

        final Instant now = Instant.now();
        final Instant minEndTime = now.plus(requestedDuration);
        final Duration auctionBuffer = Duration.ofSeconds(botSettings.get().getInteger("auctionGap"));
        final Instant bufferedEndTime = latestEndTime.plus(auctionBuffer);

        final Duration auctionDuration;
        if (bufferedEndTime.isAfter(minEndTime)) {
            auctionDuration = Duration.between(now, bufferedEndTime);
        } else {
            auctionDuration = Duration.between(now, minEndTime);
        }

        latestEndTime = Instant.now().plus(auctionDuration);
        return  auctionDuration;
    }

    Optional<Map.Entry<RunningAuction<CTX>, Long>> findAuctionEntry(
            final String identifier) {
        synchronized (runningAuctions) {
            return runningAuctions.entrySet().stream()
                    .filter(entry -> entry.getKey().getName().equalsIgnoreCase(identifier) || entry.getKey().getId().equals(identifier))
                    .findFirst();
        }
    }

    public Optional<String> lookupAuctionName(
            final String auctionId) {
        return findAuctionEntry(auctionId)
                .map(Map.Entry::getKey)
                .map(RunningAuction::getName);
    }

    public List<RunningAuction<CTX>> runningAuctions() {
        synchronized (runningAuctions) {
            return new ArrayList<>(runningAuctions.keySet());
        }
    }
}
