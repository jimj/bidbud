package net.jimj.bidbud.auction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Auction<CTX> {
    private final Loot loot;
    private final List<Bid<CTX>> bids = new ArrayList<>();

    public Auction(
            final Loot loot,
            final List<Bid<CTX>> bids) {
        this.loot = loot;
        this.bids.addAll(bids);
    }

    public Loot getLoot() {
        return loot;
    }

    public List<Bid<CTX>> getBids() {
        return new ArrayList<>(bids);
    }
}
