package net.jimj.bidbud.auction;

public record Bid<CTX>(String bidderSnowflake, String characterName, int amount, CTX context) {
}
