package net.jimj.bidbud.auction;

/**
 * Loot is auctioned and bid on.
 */
public record Loot(String name, int quantity) {
}
