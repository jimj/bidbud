package net.jimj.bidbud.cats;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;

public class CatWebClient {
    private final WebClient webClient;
    private final String apiKey;

    public CatWebClient(
            final Vertx vertx,
            final String apiKey) {
        final WebClientOptions options = new WebClientOptions()
                .setDefaultHost("api.thecatapi.com")
                .setSsl(true)
                .setDefaultPort(443);
        this.webClient = WebClient.create(vertx, options);
        this.apiKey = apiKey;
    }

    public Future<String> getCatImageLocation() {
        return webClient.get("/v1/images/search")
                .setQueryParam("mime_types", "png,jpg")
                .putHeader("x-api-key", apiKey)
                .send()
                .map(response -> response.bodyAsJsonArray().getJsonObject(0).getString("url"));
    }
}
