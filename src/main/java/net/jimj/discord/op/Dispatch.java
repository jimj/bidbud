package net.jimj.discord.op;

import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Dispatch extends OpVerticle {
    private final Logger log = LogManager.getLogger(this.getClass());

    private String selfUserId;

    public Dispatch() {
        super(OpCode.DISPATCH);
    }

    @Override
    public void start() throws Exception {
        super.start();
    }

    @Override
    void handle(
            final JsonObject event) {

        final String type = event.getString("t");
        switch (type) {
            case "READY":
                log.info("Ready with sequence {}", event.getInteger("s"));
                selfUserId = event.getJsonObject("d").getJsonObject("user").getString("id");
                break;
            case "MESSAGE_CREATE":
                final String authorId = event.getJsonObject("d").getJsonObject("author").getString("id");
                if (authorId.equals(selfUserId)) {
                    break;
                }
            default:
                getVertx().eventBus()
                        .publish("discord.dispatch." + type, event.getJsonObject("d"));
        }
    }
}
