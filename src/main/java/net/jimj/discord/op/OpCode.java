package net.jimj.discord.op;

public enum OpCode {
    DISPATCH(0), PING(1), IDENTIFY(2), RECONNECT(7), HELLO(10), PONG(11);

    private final int opCode;

    OpCode(final int opCode) {
        this.opCode = opCode;
    }

    public int opCode() {
        return opCode;
    }

    public String address() {
        return "discord.opcode." + opCode;
    }

    public static OpCode find(final int opCode) {
        for (final OpCode candidate : values()) {
            if (candidate.opCode == opCode) {
                return candidate;
            }
        }

        throw new IllegalArgumentException("Unknown OpCode: " + opCode);
    }
}
