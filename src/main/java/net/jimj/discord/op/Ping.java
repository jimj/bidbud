package net.jimj.discord.op;

import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Callable;

public class Ping extends OpVerticle {
    private static final String INTERVAL_PROPERTY = "interval";
    private final Logger logger = LogManager.getLogger(this.getClass());

    private final Callable<Void> reconnectHandler;
    private boolean pongReceived = true;
    long timerId = -1;

    public Ping(final Callable<Void> reconnectHandler) {
        super(OpCode.PING);
        this.reconnectHandler = reconnectHandler;
    }

    @Override
    public void start() throws Exception {
        super.start();
        listenFor(OpCode.PONG, this::handlePong);
    }

    @Override
    void handle(
            final JsonObject data) {
        //TODO: After READY, this needs to send a sequence number according to the docs.

        final long interval = data.getLong(INTERVAL_PROPERTY);

        if (timerId > -1) {
            logger.info("Cancelling previous Ping interval");
            vertx.cancelTimer(timerId);
            timerId = -1;
        }

        write(OpCode.PING, null);

        logger.info("Starting Ping interval of {} ms", interval);
        timerId = vertx.setPeriodic(interval, (ignored) -> {
            write(OpCode.PING, null);

            if(!pongReceived) {
                logger.warn("No PONG since last PING.  Reconnecting.");
                try {
                    reconnectHandler.call();
                } catch (final Exception e) {
                    logger.error("Reconnect failed", e);
                }
            } else {
                logger.debug("PING!");
            }
            pongReceived = false;
        });
    }

    private void handlePong(
            final JsonObject ignored) {
        logger.debug("PONG.");
        pongReceived = true;
    }

    static JsonObject event(final long interval) {
        return new JsonObject().put(INTERVAL_PROPERTY, interval);
    }
}
