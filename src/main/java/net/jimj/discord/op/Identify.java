package net.jimj.discord.op;

import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Identify extends OpVerticle {
    //https://discord.com/developers/docs/topics/gateway#gateway-intents
    private static final int GATEWAY_INTENT = 0b11011100000011;

    private final Logger logger = LogManager.getLogger(this.getClass());
    private final String authToken;

    public Identify(
            final String authToken) {
        super(OpCode.IDENTIFY);
        this.authToken = authToken;
    }

    @Override
    void handle(
            final JsonObject ignored) {
        logger.info("Identifying with intents {}", GATEWAY_INTENT);
        final JsonObject properties = new JsonObject();
        properties.put("$os", "Windows 10");  //TODO: Generate this.
        properties.put("$browser", "BidBud");
        properties.put("$device", "BidBud");

        final JsonObject identity = new JsonObject();
        identity.put("token", authToken);
        identity.put("intents", GATEWAY_INTENT);
        identity.put("properties", properties);

        write(OpCode.IDENTIFY, identity);
    }
}
