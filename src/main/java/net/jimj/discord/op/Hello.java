package net.jimj.discord.op;

import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Hello extends OpVerticle {
    private static final String INTERVAL = "heartbeat_interval";

    private final Logger logger = LogManager.getLogger(this.getClass());

    public Hello() {
        super(OpCode.HELLO);
    }

    @Override
    void handle(
            final JsonObject event) {
        final long interval = event.getLong(INTERVAL);
        logger.info("Hello!  Ping interval requested - {} ms", interval);

        emit(OpCode.PING, Ping.event(interval));
        emit(OpCode.IDENTIFY, null);
    }

    private void emit(final OpCode opCode, JsonObject data) {
        vertx.eventBus().publish(opCode.address(), data);
    }
}
