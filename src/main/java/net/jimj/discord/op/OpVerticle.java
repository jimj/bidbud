package net.jimj.discord.op;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

public abstract class OpVerticle extends AbstractVerticle {
    private final OpCode opCode;

    OpVerticle(
            final OpCode opCode) {
        this.opCode = opCode;
    }

    @Override
    public void start() throws Exception {
        listenFor(opCode, this::handle);
    }

    void listenFor(
            final OpCode opCode,
            final Handler<JsonObject> handler) {
        getVertx().eventBus()
                .<JsonObject>consumer(opCode.address(),
                        (message) -> handler.handle(message.body()));
    }

    void write(final OpCode opCode, final Object data) {
        final JsonObject payload = new JsonObject();
        payload.put("op", opCode.opCode());
        payload.put("d", data);
        payload.put("s", null);
        payload.put("t", null);

        vertx.eventBus().publish("discord.gateway", payload);
    }

    abstract void handle(JsonObject event);
}
