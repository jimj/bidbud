package net.jimj.discord.agent;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserPreferenceStorage {
    private static final String STORAGE_DIR = System.getenv("USER_PREFS_DIR");
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final Map<String, UserPreferences> allPreferences = new HashMap<>();

    private static UserPreferenceStorage instance;

    private UserPreferenceStorage() {
        if (STORAGE_DIR != null) {
            loadAll();
        }
    }

    public static synchronized UserPreferenceStorage getInstance() {
        if (instance == null) {
            instance = new UserPreferenceStorage();
        }

        return instance;
    }

    private void loadAll()  {
        final Path storageDirectory = Paths.get(STORAGE_DIR);
        try {
            Files.find(storageDirectory, 1, (path, attr) -> attr.isRegularFile())
                    .forEach(this::load);
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void load(final Path userPref) {
        final String user = userPref.getFileName().toString();

        final byte[] bytes;
        try {
            bytes = Files.readAllBytes(userPref);
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }

        final Buffer buffer = Buffer.buffer(bytes);
        final JsonObject preferences = new JsonObject(buffer);
        allPreferences.put(user, new UserPreferences(preferences));
        logger.info("Read preferences for {}", user);
    }


    public UserPreferences loadPreferences(
            final String userId) {
        return allPreferences.getOrDefault(userId, UserPreferences.defaultPreferences());
    }

    public void savePreference(
            final String userId,
            final String preferenceName,
            final String preferenceValue) {

        final JsonObject updatedPreferences = loadPreferences(userId).save();
        updatedPreferences.put(preferenceName, preferenceValue);
        allPreferences.put(userId, new UserPreferences(updatedPreferences));

        final Path persistedPreferences = Paths.get(STORAGE_DIR, userId);
        try {
            Files.writeString(persistedPreferences, updatedPreferences.toString());
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public <T> void savePreference(
            final String userId,
            final String preferenceName,
            final List<T> preferenceValues) {

        final JsonObject updatedPreferences = loadPreferences(userId).save();
        updatedPreferences.put(preferenceName, new JsonArray(preferenceValues));
        allPreferences.put(userId, new UserPreferences(updatedPreferences));

        final Path persistedPreferences = Paths.get(STORAGE_DIR, userId);
        try {
            Files.writeString(persistedPreferences, updatedPreferences.toString());
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
