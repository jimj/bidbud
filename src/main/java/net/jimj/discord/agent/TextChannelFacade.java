package net.jimj.discord.agent;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import net.jimj.discord.agent.response.ActionRow;
import net.jimj.discord.agent.response.Embed;
import net.jimj.discord.agent.response.ImmutableTextMessage;
import net.jimj.discord.agent.response.InteractionCallbackType;
import net.jimj.discord.agent.response.InteractionResponse;
import net.jimj.discord.agent.response.MessageInteraction;
import net.jimj.discord.agent.response.TextMessage;
import net.jimj.discord.vertx.DiscordWebClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class TextChannelFacade {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final String id;
    private final DiscordWebClient webClient;

    TextChannelFacade(
            final DiscordWebClient webClient,
            final String id) {
        this.webClient = webClient;
        this.id = id;
    }

    String getMessageEndpoint() {
        return "/channels/" + id + "/messages";
    }

    public Future<MessageFacade> send(
            final String message,
            final List<ActionRow> components) {

        final TextMessage textMessage = ImmutableTextMessage.builder()
                .content(message)
                .components(components)
                .build();

        return send(textMessage);
    }


    public Future<MessageFacade> send(
            final String message) {

        return send(ImmutableTextMessage.builder().content(message).build());
    }

    public Future<MessageFacade> send(
            final Embed embed) {
        return send(ImmutableTextMessage.builder().embed(embed).build());
    }

    public Future<MessageFacade> send(
            final TextMessage message) {

        final Promise<MessageFacade> handle = Promise.promise();
        final Future<HttpResponse<Buffer>> responseFuture;

        if (message.attachments().isEmpty()) {
            responseFuture = webClient.post(getMessageEndpoint())
                    .sendBuffer(Buffer.buffer(message.toJson().encode()));
        } else {
            responseFuture = webClient.post(getMessageEndpoint())
                    .putHeader("content-type", "multipart/form-data")
                    .sendMultipartForm(message.toForm());
        }

        responseFuture.onSuccess(result -> {
            final JsonObject serverResponse = result.bodyAsJsonObject();
            handle.complete(new MessageFacade(webClient, this, serverResponse.getString("id")));
        })
        .onFailure(err -> {
            logger.error("Failed: ", err);
            handle.fail(err);
        });


        return handle.future();
    }
}
