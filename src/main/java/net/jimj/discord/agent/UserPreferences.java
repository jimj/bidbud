package net.jimj.discord.agent;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;

public class UserPreferences {
    private final JsonObject preferences;

    public UserPreferences(
            final JsonObject preferences) {
        this.preferences = preferences.copy();
    }

    JsonObject save() {
        return preferences;
    }

    public static UserPreferences defaultPreferences() {
        var defaultPreferences = new JsonObject();
        return new UserPreferences(defaultPreferences);
    }

    public String getString(final String name) {
        return preferences.getString(name);
    }

    public List<String> getStrings(final String name) {
        return preferences.getJsonArray(name, new JsonArray())
                .stream()
                .map(val -> (String)val)
                .filter(val -> !val.isBlank())
                .toList();
    }
}
