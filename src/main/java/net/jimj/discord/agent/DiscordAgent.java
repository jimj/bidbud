package net.jimj.discord.agent;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import net.jimj.discord.agent.interactions.ImmutableInteractionContext;
import net.jimj.discord.agent.interactions.InteractionContext;
import net.jimj.discord.agent.interactions.InteractionFacade;
import net.jimj.discord.agent.interactions.InteractionOptions;
import net.jimj.discord.agent.interactions.InteractionRegistry;
import net.jimj.discord.agent.interactions.InteractionType;
import net.jimj.discord.agent.interactions.TextInput;
import net.jimj.discord.agent.response.StatefulComponent;
import net.jimj.discord.vertx.DiscordWebClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class DiscordAgent {
    private final Logger logger = LogManager.getLogger(this.getClass());

    private final HashMap<MessageSelector, Consumer<MessageContext>> messageHandlers = new HashMap<>();
    private final InteractionRegistry interactionRegistry = InteractionRegistry.instance();
    private final Vertx vertx;
    private final DiscordWebClient client;
    private final Session session;
    private final UserPreferenceStorage userPreferenceStorage = UserPreferenceStorage.getInstance();

    public DiscordAgent(
            final Vertx vertx,
            final DiscordWebClient client,
            final Session session) {
        this.vertx = vertx;
        this.client = client;
        this.session = session;

        vertx.eventBus()
                .<JsonObject>consumer("discord.dispatch.MESSAGE_CREATE", (message) -> processMessage(message.body()));
        vertx.eventBus()
                .<JsonObject>consumer("discord.dispatch.INTERACTION_CREATE", (message) -> processInteraction(message.body()));
    }

    public Vertx getVertx() {
        return vertx;
    }

    public void onMessage(
            final MessageSelector messageSelector,
            final Consumer<MessageContext> handler) {
        messageHandlers.put(messageSelector, handler);
    }

    private void processMessage(
            final JsonObject messageData) {

        final String channelId = messageData.getString("channel_id");
        final TextChannelFacade channelFacade = new TextChannelFacade(client, channelId);
        final UserContext userContext = toUserContext(messageData.getJsonObject("author"));
        final MessageContext context = ImmutableMessageContext.builder()
                .channel(channelFacade)
                .message(messageData.getString("content"))
                .userContext(userContext)
                .build();

        messageHandlers.forEach((selector, handler) -> {
            if (selector.matches(messageData)) {
                handler.accept(context);
            }
        });
    }

    private void processInteraction(
            final JsonObject interaction) {
        final InteractionType interactionType = InteractionType.values()[interaction.getInteger("type")];
        final String interactionId = interaction.getString("id");
        final InteractionFacade interactionFacade = new InteractionFacade(
                client,
                interaction.getString("application_id"),
                interactionId,
                interaction.getString("token"));

        logger.debug("Remembered [{}] interaction {}", interactionType, interactionId);
        session.rememberInteraction(interactionId, interactionFacade);

        logger.trace("Processing interaction {}", interaction.encodePrettily());
        final InteractionContext interactionContext = createInteractionContext(
                interactionType,
                interactionFacade,
                interaction);

        invokeInteractionHandler(
                interaction,
                interactionContext);
    }

    private InteractionContext createInteractionContext(
            final InteractionType interactionType,
            final InteractionFacade interactionFacade,
            final JsonObject interaction) {

        final ImmutableInteractionContext.Builder contextBuilder = ImmutableInteractionContext.builder()
                .type(interactionType)
                .interaction(interactionFacade);

        final JsonObject user = interaction.containsKey("member")
                ? interaction.getJsonObject("member").getJsonObject("user")
                : interaction.getJsonObject("user");
        contextBuilder.userContext(toUserContext(user));

        if (interactionType == InteractionType.MESSAGE_COMPONENT) {
            final JsonObject message = interaction.getJsonObject("message");
            if (message.containsKey("interaction")) {
                contextBuilder.parentInteraction(session.recallInteraction(message.getJsonObject("interaction").getString("id")));
            }

            contextBuilder.options(new InteractionOptions(new JsonArray()));
        } else {
            contextBuilder.options(new InteractionOptions(interaction.getJsonObject("data").getJsonArray("options")));
        }

        return contextBuilder.build();
    }

    private void invokeInteractionHandler(
            final JsonObject interaction,
            final InteractionContext context) {

        final JsonObject interactionData = interaction.getJsonObject("data");

        switch (context.type()) {
            case APPLICATION_COMMAND -> {
                final String interactionId = interactionData.getString("name");
                logger.debug("Command Interaction [{}]", interactionId);
                interactionRegistry
                        .findCommand(interactionId)
                        .ifPresentOrElse(
                                handler -> handler.onInvoke(context),
                                () -> logger.warn("No handler for command {}: {}", interactionId, interaction));
            }

            case APPLICATION_COMMAND_AUTOCOMPLETE -> {
                final String interactionId = interactionData.getString("name");
                logger.debug("AutoComplete Interaction [{}]", interactionId);
                final Optional<String> focusedOption = InteractionOptions.getFocusedOption(interactionData.getJsonArray("options"));

                if (focusedOption.isEmpty()) {
                    return;
                }

                interactionRegistry
                        .findCommand(interactionId)
                        .ifPresentOrElse(
                                handler -> handler.onAutocomplete(focusedOption.get(), context),
                                () -> logger.warn("No handler for command {}: {}", interactionId, interaction));

            }

            case MESSAGE_COMPONENT -> {
                final StatefulComponent component = StatefulComponent.deserialize(interactionData);
                logger.debug("Component Interaction [{}]", component.id());
                interactionRegistry
                        .findComponent(component.id())
                        .ifPresentOrElse(
                                handler -> handler.onInvoke(component, context, interactionData),
                                () -> logger.warn("No handler for component {}: {}", component.id(), interaction));
            }

            case MODAL_SUBMIT -> {
                final StatefulComponent component = StatefulComponent.deserialize(interactionData);
                final List<TextInput> inputs = TextInput.extract(interactionData.getJsonArray("components"));
                logger.debug("Modal Interaction [{}]", component.id());
                interactionRegistry
                        .findModal(component.id())
                        .ifPresentOrElse(
                                handler -> handler.onInvoke(inputs, component, context),
                                () -> logger.warn("No handler for component {}: {}", component.id(), interaction));
            }

            default -> logger.warn("Unhandled Interaction type {}", context.type());
        }
    }

    private UserContext toUserContext(final JsonObject user) {
        final String userName = String.format(
                "%s#%s",
                user.getString("username"),
                user.getString("discriminator"));
        final String userSnowflake = user.getString("id");
        final UserPreferences preferences = userPreferenceStorage.loadPreferences(userSnowflake);

        return ImmutableUserContext.builder()
                .name(userName)
                .snowflake(userSnowflake)
                .preferences(preferences)
                .build();
    }
}
