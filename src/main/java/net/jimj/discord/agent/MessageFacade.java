package net.jimj.discord.agent;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import net.jimj.discord.agent.response.Attachment;
import net.jimj.discord.agent.response.Embed;
import net.jimj.discord.agent.response.ImmutableTextMessage;
import net.jimj.discord.agent.response.TextMessage;
import net.jimj.discord.vertx.DiscordWebClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class MessageFacade {
    private final DiscordWebClient webClient;
    private final String endpoint;

    private final Logger logger = LogManager.getLogger(this.getClass());

    MessageFacade(
            final DiscordWebClient webClient,
            final TextChannelFacade channel,
            final String id) {
        this.webClient = webClient;
        this.endpoint = channel.getMessageEndpoint() + "/" + id;
    }

    public Future<Void> update(
            final String message) {
        final Promise<Void> promise = Promise.promise();

        final TextMessage updatedMessage = ImmutableTextMessage.builder()
                .content(message)
                .build();

        webClient.patch(endpoint)
                .sendBuffer(Buffer.buffer(updatedMessage.toJson().encode()))
                .onSuccess((ignored) -> promise.complete(null))
                .onFailure(promise::fail);

        return promise.future();
    }

    public Future<?> update(
            final Embed embed,
            final List<Attachment> attachments) {

        final TextMessage updatedMessage = ImmutableTextMessage.builder()
                .embed(embed)
                .attachments(attachments)
                .build();

        if (updatedMessage.attachments().isEmpty()) {
            return webClient.patch(endpoint)
                    .sendBuffer(Buffer.buffer(updatedMessage.toJson().encode()));
        } else {
            return webClient.patch(endpoint)
                    .putHeader("content-type", "multipart/form-data")
                    .sendMultipartForm(updatedMessage.toForm());
        }
    }

    public void delete() {
        webClient.delete(endpoint)
                .send();
    }
}
