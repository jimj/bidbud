package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonObject;

public record InteractionResponse<T extends JsonSerializable>(
        InteractionCallbackType type,
        T data
) {
    public JsonObject toJson() {
        final JsonObject interaction = new JsonObject();
        interaction.put("type", type.apiValue);
        interaction.put("data", data.toJson());

        return interaction;
    }
}
