package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonObject;

public record EmptyInteraction() implements JsonSerializable {
    @Override
    public JsonObject toJson() {
        return new JsonObject();
    }
}
