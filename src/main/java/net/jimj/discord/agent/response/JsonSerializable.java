package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonObject;

public interface JsonSerializable {
    JsonObject toJson();
}
