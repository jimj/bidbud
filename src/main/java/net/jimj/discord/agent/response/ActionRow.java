package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ActionRow implements Component {
    private final List<Component> components = new ArrayList<>();

    public ActionRow(
            final Component... components) {
        Arrays.stream(components)
                .forEach(this::addComponent);
    }

    @Override
    public Type type() {
        return Type.ACTION_ROW;
    }

    @Override
    public JsonObject toJson() {
        final JsonObject json = new JsonObject();
        json.put("type", type().toApiValue());

        if (!components.isEmpty()) {
            final JsonArray jsonComponents = new JsonArray();
            components.stream()
                    .map(Component::toJson)
                    .forEach(jsonComponents::add);
            json.put("components", jsonComponents);
        }

        return json;
    }

    void addComponent(
            final Component component) {
        if (component.type() == Type.ACTION_ROW) {
            throw new IllegalArgumentException("An ActionRow component cannot contain other ActionRow components.");
        }

        components.add(component);
    }
}
