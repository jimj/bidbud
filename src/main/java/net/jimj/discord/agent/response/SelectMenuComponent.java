package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
public interface SelectMenuComponent extends Component, StatefulComponent {
    List<SelectOption> options();

    @Override
    default Type type() {
        return Type.SELECT_MENU;
    }

    @Override
    default JsonObject toJson() {
        final JsonObject result = stateToJson()
                .put("type", type().toApiValue());

        final JsonArray options = new JsonArray();
        options().stream()
                .map(SelectOption::toJson)
                .forEach(options::add);

        result.put("options", options);

        return result;
    }
}
