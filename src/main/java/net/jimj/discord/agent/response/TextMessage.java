package net.jimj.discord.agent.response;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.multipart.MultipartForm;
import org.immutables.value.Value;

import java.util.List;
import java.util.Optional;

@Value.Immutable
public interface TextMessage {
    Optional<String> content();
    Optional<Embed> embed();
    List<Attachment> attachments();
    List<Component> components();

    default JsonObject toJson() {
        final JsonObject messageBody = new JsonObject();
        messageBody.put("tts", false);

        content().ifPresent(content -> messageBody.put("content", content));
        embed().ifPresent(embed -> messageBody.put("embeds", new JsonArray().add(embed.toJson())));
        if (!attachments().isEmpty()) {
            final JsonArray attachments = new JsonArray();
            attachments().stream()
                    .map(Attachment::toJson)
                    .forEach(attachments::add);
            messageBody.put("attachments", attachments);
        }

        if (!components().isEmpty()) {
            final JsonArray components = new JsonArray();
            components().stream()
                    .map(Component::toJson)
                    .forEach(components::add);
            messageBody.put("components", components);
        }

        return messageBody;
    }

    default MultipartForm toForm() {
        final MultipartForm form =  MultipartForm.create()
                .textFileUpload("payload_json", "", Buffer.buffer(toJson().toString()), Attachment.MediaType.JSON.mimeType);

        attachments().forEach(attachment -> {
            final String name = String.format("files[%s]", attachment.id());
            form.binaryFileUpload(name, attachment.name(), attachment.content(), attachment.mediaType().mimeType);
        });

        return form;
    }
}
