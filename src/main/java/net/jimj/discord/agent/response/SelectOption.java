package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonObject;

public record SelectOption(
        String label,
        String value,
        boolean selected) {

    JsonObject toJson() {
        return new JsonObject()
                .put("label", label)
                .put("value", value)
                .put("default", selected);
    }
}
