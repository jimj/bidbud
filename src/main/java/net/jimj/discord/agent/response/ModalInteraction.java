package net.jimj.discord.agent.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;

public record ModalInteraction(
        String title,
        String id,
        @JsonIgnore JsonObject state,
        List<? extends Component> components) implements StatefulComponent, JsonSerializable {

    @Override
    public JsonObject toJson() {
        final JsonObject interactionData = stateToJson()
                .put("title", title());

        if (!components().isEmpty()) {
            final JsonArray components = new JsonArray();
            components().stream()
                    .map(Component::toJson)
                    .forEach(components::add);
            interactionData.put("components", components);
        }

        return interactionData;
    }
}
