package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonObject;
import org.immutables.value.Value;

@Value.Immutable
public interface AutocompleteChoice {
    String name();
    String value();

    default JsonObject toJson() {
        return new JsonObject()
                .put("name", name())
                .put("value", value());
    }
}
