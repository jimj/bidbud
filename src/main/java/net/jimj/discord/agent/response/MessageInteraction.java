package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public record MessageInteraction(
        Optional<String> content,
        Optional<Embed> embed,
        List<? extends Component> components,
        List<Attachment> attachments,
        boolean ephemeral) implements JsonSerializable {

    public static MessageInteraction message(
            final String message) {
        return new MessageInteraction(
                Optional.of(message),
                Optional.empty(),
                Collections.emptyList(),
                Collections.emptyList(),
                true);
    }

    @Override
    public JsonObject toJson() {
        final JsonObject interactionData = new JsonObject();

        if (ephemeral()) {
            interactionData.put("flags", 1 << 6);
        }

        interactionData.put("tts", false);

        content().ifPresent(content -> interactionData.put("content", content));
        embed().ifPresent(embed -> interactionData.put("embeds", new JsonArray().add(embed.toJson())));

        if (!components().isEmpty()) {
            final JsonArray components = new JsonArray();
            components().stream()
                    .map(Component::toJson)
                    .forEach(components::add);
            interactionData.put("components", components);
        }

        if (!attachments().isEmpty()) {
            final JsonArray attachments = new JsonArray();
            attachments().stream()
                    .map(Attachment::toJson)
                    .forEach(attachments::add);
            interactionData.put("attachments", attachments);
        }

        return interactionData;
    }
}
