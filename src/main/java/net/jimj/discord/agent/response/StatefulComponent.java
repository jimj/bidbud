package net.jimj.discord.agent.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Locale;
import java.util.StringJoiner;

public interface StatefulComponent {
    @JsonProperty("custom_id")
    String id();
    JsonObject state();

    default JsonObject stateToJson() {
        final StringJoiner idBuilder = new StringJoiner(":");
        idBuilder.add(id());

        final JsonObject stateWithoutId = state().copy();
        stateWithoutId.remove("custom_id");
        final String encodedState = Base64.getEncoder().encodeToString(stateWithoutId.encode().getBytes(StandardCharsets.UTF_8));
        idBuilder.add(encodedState);

        return new JsonObject()
                .put("custom_id", idBuilder.toString());
    }

    static StatefulComponent deserialize(
            final JsonObject componentData) {

        LogManager.getLogger(StatefulComponent.class)
                .debug("Deserializing {}", componentData);
        final String customId = componentData.getString("custom_id");
        final String[] idParts = customId.split(":");

        final String componentId = idParts[0];
        final JsonObject state;
        if (idParts.length == 1) {
            state = new JsonObject();
        } else {
            final byte[] decodedState = Base64.getDecoder().decode(idParts[1]);
            state = new JsonObject(Buffer.buffer(decodedState));
        }

        return new StatefulComponent() {
            @Override
            public String id() {
                return componentId;
            }

            @Override
            public JsonObject state() {
                return state;
            }
        };
    }
}
