package net.jimj.discord.agent.response;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import org.immutables.value.Value;

@Value.Immutable
public interface Attachment {
    String id();
    String name();
    String fileName();
    MediaType mediaType();
    Buffer content();

    enum MediaType {
        JSON("application/json"),
        JPG("image/jpeg"),
        PNG("image/png");

        final String mimeType;
        MediaType(final String mimeType) {
            this.mimeType = mimeType;
        }

        String getMimeType() {
            return mimeType;
        }
    }

    default JsonObject toJson() {
        return new JsonObject()
                .put("id", id())
                .put("filename", fileName());
    }
}
