package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonObject;
import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
public interface TextInputComponent extends Component, StatefulComponent {
    enum Style {
        SHORT, PARAGRAPH;
    }

    Style style();
    String label();
    boolean required();
    Optional<String> value();
    Optional<String> placeholder();

    @Override
    default Type type() {
        return Type.TEXT_INPUT;
    }

    @Override
    @Value.Default
    default JsonObject state() {
        return new JsonObject();
    }

    @Override
    default JsonObject toJson() {
        final JsonObject result = stateToJson()
                .put("type", type().toApiValue())
                .put("style", style().ordinal() + 1)
                .put("required", required())
                .put("label", label());

        value().ifPresent(value -> result.put("value", value));
        placeholder().ifPresent(placeholder -> result.put("placeholder", placeholder));

        return result;
    }
}
