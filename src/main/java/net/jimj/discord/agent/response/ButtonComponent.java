package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonObject;
import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
public interface ButtonComponent extends Component, StatefulComponent {
    enum Style {
        PRIMARY, SECONDARY, SUCCESS, DANGER, LINK
    }

    @Override
    default Type type() {
        return Type.BUTTON;
    }

    Style style();
    Optional<String> label();
    Optional<String> url();

    @Value.Default
    default boolean disabled() {
        return false;
    }

    @Override
    default JsonObject toJson() {
        final JsonObject result = stateToJson()
                .put("type", type().toApiValue())
                .put("disabled", disabled())
                .put("style", style().ordinal() + 1);

        label().ifPresent(label -> result.put("label", label));
        url().ifPresent(url -> result.put("url", url));

        return result;
    }
}
