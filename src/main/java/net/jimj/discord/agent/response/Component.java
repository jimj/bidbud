package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonObject;

public interface Component {
    enum Type {
        ACTION_ROW, BUTTON, SELECT_MENU, TEXT_INPUT;

        int toApiValue() {
            return this.ordinal() + 1;
        }

        public static Type fromApiValue(final int value) {
            return Type.values()[value - 1];
        }
    }

    Type type();
    JsonObject toJson();
}
