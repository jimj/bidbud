package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.immutables.value.Value;

import java.util.Map;
import java.util.Optional;

@Value.Immutable
public interface Embed {
    Optional<String> title();
    Optional<String> description();
    Optional<String> thumbnail();
    Optional<String> image();
    Optional<String> footer();
    Optional<String> url();
    Map<String, String> fields();

    default JsonObject toJson() {
        final JsonObject embed = new JsonObject();

        title().ifPresent(title -> embed.put("title", title));
        description().ifPresent(description -> embed.put("description", description));
        thumbnail().ifPresent(thumbnail -> embed.put("thumbnail", new JsonObject().put("url", thumbnail)));
        image().ifPresent(image -> embed.put("image", new JsonObject().put("url", image)));
        footer().ifPresent(footer -> embed.put("footer", new JsonObject().put("text", footer)));
        url().ifPresent(url -> embed.put("url", url));

        if (!fields().isEmpty()) {
            final JsonArray fields = new JsonArray();
            fields().forEach((k, v) -> fields.add(new JsonObject().put("name", k).put("value", v)));
            embed.put("fields", fields);
        }

        return embed;
    }
}
