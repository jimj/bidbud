package net.jimj.discord.agent.response;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;

public record AutoCompleteInteraction(
        List<AutocompleteChoice> choices) implements JsonSerializable {

    @Override
    public JsonObject toJson() {
        final JsonObject interactionData = new JsonObject();

        if (!choices().isEmpty()) {
            final JsonArray choices = new JsonArray();
            choices().stream()
                    .map(AutocompleteChoice::toJson)
                    .forEach(choices::add);
            interactionData.put("choices", choices);
        }

        return interactionData;
    }
}
