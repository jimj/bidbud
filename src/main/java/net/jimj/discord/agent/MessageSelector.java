package net.jimj.discord.agent;

import io.vertx.core.json.JsonObject;

@FunctionalInterface
public interface MessageSelector {
    boolean matches(JsonObject message);

    default MessageSelector and(
            final MessageSelector other) {
        return message -> matches(message) && other.matches(message);
    }

    default MessageSelector or(
            final MessageSelector other) {
        return message -> matches(message) || other.matches(message);
    }
}
