package net.jimj.discord.agent;

import io.vertx.core.json.JsonObject;

import java.util.function.Predicate;

public class MessageSelectors {
    public static MessageSelector toGuildId(
            final String snowflake) {
        return (message) -> snowflake.equals(message.getString("guild_id"));
    }

    public static MessageSelector toChannelId(
            final String snowflake) {
        return (message) -> snowflake.equals(message.getString("channel_id"));
    }

    public static MessageSelector contentMatches(
            final Predicate<String> matcher) {
        return (message) -> matcher.test(message.getString("content"));
    }

    public static MessageSelector isPrivate() {
        return (message) -> !message.containsKey("guild_id");
    }

    public static boolean anyOf(
            final JsonObject message,
            final MessageSelector... selectors) {
        for (final MessageSelector selector : selectors) {
            if (selector.matches(message)) {
                return true;
            }
        }
        return false;
    }
}
