package net.jimj.discord.agent;

import net.jimj.discord.agent.interactions.InteractionFacade;

import java.util.Optional;

public interface Session {
    void rememberInteraction(final String id, final InteractionFacade interaction);
    Optional<InteractionFacade> recallInteraction(final String id);
}
