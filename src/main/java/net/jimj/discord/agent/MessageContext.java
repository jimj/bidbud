package net.jimj.discord.agent;

import org.immutables.value.Value;

@Value.Immutable
public interface MessageContext {
    TextChannelFacade channel();
    String message();
    UserContext userContext();
}
