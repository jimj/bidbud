package net.jimj.discord.agent;

import io.vertx.core.AbstractVerticle;
import net.jimj.discord.agent.interactions.InteractionFacade;
import net.jimj.discord.vertx.EntryExpiringMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.util.Map;
import java.util.Optional;

public class SessionVerticle extends AbstractVerticle implements Session {
    private final Logger logger = LogManager.getLogger(this.getClass());

    private Map<String, InteractionFacade> previousInteractions;

    @Override
    public void start() throws Exception {
        super.start();

        previousInteractions = new EntryExpiringMap<>(
                getVertx(),
                Duration.ofMinutes(10),
                Duration.ofMinutes(1));
    }

    @Override
    public void rememberInteraction(
            final String id,
            final InteractionFacade interaction) {
        previousInteractions.put(id, interaction);
    }

    @Override
    public Optional<InteractionFacade> recallInteraction(String id) {
        return Optional.ofNullable(previousInteractions.get(id));
    }
}
