package net.jimj.discord.agent;

//https://discord.com/developers/docs/resources/channel#channel-object-channel-types
//ordinal value matches id
enum ChannelType {
    GUILD_TEXT, DM, GUILD_VOICE, GROUP_DM, GUILD_CATEGORY, GUILD_NEWS, GUILD_STORE
}
