package net.jimj.discord.agent;

import xyz.downgoon.snowflake.Snowflake;

public class Snowflakes {
    private static final Snowflake ATTACHMENT = new Snowflake(1, 1);

    public static String attachment() {
        return Long.toString(ATTACHMENT.nextId());
    }
}
