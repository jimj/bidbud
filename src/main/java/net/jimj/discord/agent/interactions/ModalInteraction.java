package net.jimj.discord.agent.interactions;

import net.jimj.discord.agent.response.StatefulComponent;

import java.util.List;

public interface ModalInteraction extends Interaction {
    void onInvoke(List<TextInput> modalInputs, StatefulComponent component, InteractionContext interactionContext);
}
