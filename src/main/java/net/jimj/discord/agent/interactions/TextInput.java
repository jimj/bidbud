package net.jimj.discord.agent.interactions;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import net.jimj.discord.agent.response.Component;
import net.jimj.discord.agent.response.StatefulComponent;

import java.util.ArrayList;
import java.util.List;

public record TextInput(String name, String value) {
    /**
     * Modals callbacks include a list of input components.
     * This method will unwrap all components found within ActionRow components and return
     * them in a flat list.
     */
    public static List<TextInput> extract(
            final JsonArray components) {
        final List<TextInput> extractedComponents = new ArrayList<>();

        for (int i = 0; i < components.size(); i++) {
            final JsonObject component = components.getJsonObject(i);
            if (Component.Type.fromApiValue(component.getInteger("type")) == Component.Type.ACTION_ROW) {
                extractedComponents.addAll(
                        extract(component.getJsonArray("components")));
            } else {
                extractedComponents.add(readValue(component));
            }
        }

        return extractedComponents;
    }

    public static TextInput readValue(
            final JsonObject component) {

        final StatefulComponent statefulComponent = StatefulComponent.deserialize(component);
        final String id = statefulComponent.id();
        final String value = component.getString("value");

        return new TextInput(id, value);
    }
}
