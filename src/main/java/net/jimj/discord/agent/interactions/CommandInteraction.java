package net.jimj.discord.agent.interactions;

public interface CommandInteraction extends Interaction {
    void onInvoke(InteractionContext interactionContext);
    void onAutocomplete(String name, InteractionContext interactionContext);
}
