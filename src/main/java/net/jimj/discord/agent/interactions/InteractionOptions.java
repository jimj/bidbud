package net.jimj.discord.agent.interactions;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Optional;

public class InteractionOptions {
    private final String subCommandName;
    private final InteractionOptions subCommandOptions;
    private final JsonObject container = new JsonObject();

    public InteractionOptions(
            final JsonArray options) {

        if (options != null
                && options.size() == 1
                && CommandOptionType.fromJson(options.getJsonObject(0)) == CommandOptionType.SUB_COMMAND) {

            final JsonObject subCommand = options.getJsonObject(0);
            subCommandName = subCommand.getString("name");
            subCommandOptions = subCommand.containsKey("options")
                    ? new InteractionOptions(subCommand.getJsonArray("options"))
                    : null;
        } else {
            subCommandName = null;
            subCommandOptions = null;

            if (options != null) {
                for (int i = 0; i < options.size(); i++) {
                    final JsonObject option = options.getJsonObject(i);
                    container.put(option.getString("name"), option.getValue("value"));
                }
            }
        }
    }

    public static Optional<String> getFocusedOption(
            final JsonArray options) {

        if (options != null) {
            for (int i = 0; i < options.size(); i++) {
                final JsonObject option = options.getJsonObject(i);
                if (option.containsKey("focused") && option.getBoolean("focused")) {
                    return Optional.ofNullable(option.getString("name"));
                }
            }
        }

        return Optional.empty();
    }

    public boolean hasOption(final String name) {
        return container.containsKey(name);
    }

    public int getIntValue(final String optionName) {
        return container.getInteger(optionName);
    }

    public String getStringValue(final String optionName) {
        return container.getString(optionName);
    }

    public boolean getBoolValue(final String optionName) {
        return container.getBoolean(optionName);
    }

    public InteractionOptions getSubCommandOptions() {
        return subCommandOptions;
    }

    public String getSubCommandName() {
        return subCommandName;
    }

    public boolean isSubCommand() {
        return subCommandName != null;
    }
}
