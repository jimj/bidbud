package net.jimj.discord.agent.interactions;

public interface Interaction {
    String id();
    void accept(InteractionRegistry registry);
}
