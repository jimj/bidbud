package net.jimj.discord.agent.interactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Interaction handlers must register themselves with the registry.
 */
public final class InteractionRegistry {
    private static InteractionRegistry INSTANCE;

    private final Logger logger = LogManager.getLogger(this.getClass());
    private final Map<String, CommandInteraction> commands = new HashMap<>();
    private final Map<String, ComponentInteraction> components = new HashMap<>();
    private final Map<String, ModalInteraction> modals = new HashMap<>();

    private InteractionRegistry() {

    }

    public static InteractionRegistry instance() {
        if (INSTANCE == null) {
            INSTANCE = new InteractionRegistry();
        }

        return INSTANCE;
    }

    public Optional<CommandInteraction> findCommand(
            final String id) {
        return Optional.ofNullable(commands.get(id));
    }

    public Optional<ComponentInteraction> findComponent(
            final String id) {
        return Optional.ofNullable(components.get(id));
    }

    public Optional<ModalInteraction> findModal(
            final String id) {
        return Optional.ofNullable(modals.get(id));
    }

//    /**
//     * Public entry point.  Call this to register any new interaction.
//     */
//    public void visit(
//            final Interaction interaction) {
//        interaction.accept(this);
//    }

    /**
     * Interaction implementations should call this during an {{@link Interaction#accept(InteractionRegistry)}} call.
     * i.e. 'this' should be the only argument passed to this function.
     */
    public void registerCommand(
            final CommandInteraction command) {
        commands.compute(command.id(), (id, existingHandler) -> {
            if(existingHandler != null) {
                logger.warn("Overwriting [{}] interaction {} with {}", id, existingHandler.getClass(), command.getClass());
            }
            return command;
        });
    }

    /**
     * Interaction implementations should call this during an {{@link Interaction#accept(InteractionRegistry)}} call.
     * i.e. 'this' should be the only argument passed to this function.
     */
    public void registerModal(
            final ModalInteraction modal) {
        modals.compute(modal.id(), (id, existingHandler) -> {
            if(existingHandler != null) {
                logger.warn("Overwriting [{}] interaction {} with {}", id, existingHandler.getClass(), modal.getClass());
            }

            return modal;
        });
    }

    /**
     * Interaction implementations should call this during an {{@link Interaction#accept(InteractionRegistry)}} call.
     * i.e. 'this' should be the only argument passed to this function.
     */
    public void registerComponent(
            final ComponentInteraction component) {
        components.compute(component.id(), (id, existingHandler) -> {
            if(existingHandler != null) {
                logger.warn("Overwriting [{}] interaction {} with {}", id, existingHandler.getClass(), component.getClass());
            }

            return component;
        });
    }
}
