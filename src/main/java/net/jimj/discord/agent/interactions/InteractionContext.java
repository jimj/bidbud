package net.jimj.discord.agent.interactions;

import net.jimj.discord.agent.UserContext;
import org.immutables.value.Value;

import java.util.Optional;

@Value.Immutable
public interface InteractionContext {
    InteractionType type();
    InteractionFacade interaction();
    UserContext userContext();
    InteractionOptions options();
    Optional<InteractionFacade> parentInteraction();
}
