package net.jimj.discord.agent.interactions;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import net.jimj.discord.agent.response.ActionRow;
import net.jimj.discord.agent.response.AutoCompleteInteraction;
import net.jimj.discord.agent.response.AutocompleteChoice;
import net.jimj.discord.agent.response.EmptyInteraction;
import net.jimj.discord.agent.response.InteractionCallbackType;
import net.jimj.discord.agent.response.InteractionResponse;
import net.jimj.discord.agent.response.JsonSerializable;
import net.jimj.discord.agent.response.MessageInteraction;
import net.jimj.discord.agent.response.ModalInteraction;
import net.jimj.discord.agent.response.TextInputComponent;
import net.jimj.discord.vertx.DiscordWebClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class InteractionFacade {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final String applicationId;
    private final String id;
    private final String token;
    private final DiscordWebClient webClient;

    public InteractionFacade(
            final DiscordWebClient webClient,
            final String applicationId,
            final String id,
            final String token) {
        this.webClient = webClient;
        this.applicationId = applicationId;
        this.id = id;
        this.token = token;
    }

    private String getCallbackEndpoint() {
        return "/interactions/" + id + "/" + token + "/callback";
    }

    private String getOriginalMessageEndpoint() {
        return "/webhooks/" + applicationId + "/" + token + "/messages/@original";
    }

    public Future<Void> autocomplete(final List<AutocompleteChoice> choices) {
        final InteractionResponse<AutoCompleteInteraction> response = new InteractionResponse<>(
                InteractionCallbackType.APPLICATION_COMMAND_AUTOCOMPLETE_RESULT,
                new AutoCompleteInteraction(choices));

        return post(response);
    }

    public Future<Void> modal(
            final String id,
            final String title,
            final JsonObject state,
            final TextInputComponent... inputs) {

        // Discord limit, will result in Bad Request if there are more.
        if (inputs.length > 5) {
            throw new IllegalArgumentException("Too many inputs.");
        }

        // Each input must be wrapped in an action row.
        final List<ActionRow> callbackComponents = Arrays.stream(inputs)
                .map(ActionRow::new)
                .toList();

        final InteractionResponse<ModalInteraction> response = new InteractionResponse<>(
                InteractionCallbackType.MODAL,
                new ModalInteraction(title, id, state, callbackComponents));

        return post(response);
    }

    public Future<Void> reply(
            final String message) {

        final InteractionResponse<MessageInteraction> response = new InteractionResponse<>(
                InteractionCallbackType.CHANNEL_MESSAGE_WITH_SOURCE,
                MessageInteraction.message(message));

        return post(response);
    }

    public Future<Void> reply(
            final String message,
            final List<ActionRow> components) {

        final MessageInteraction interactionData = new MessageInteraction(
                Optional.of(message),
                Optional.empty(),
                components,
                Collections.emptyList(),
                true);

        final InteractionResponse<MessageInteraction> response = new InteractionResponse<>(
                InteractionCallbackType.CHANNEL_MESSAGE_WITH_SOURCE,
                interactionData);

        return post(response);
    }

    public Future<Void> acknowledge() {
        final InteractionResponse<EmptyInteraction> response = new InteractionResponse<>(
                InteractionCallbackType.DEFERRED_UPDATE_MESSAGE,
                new EmptyInteraction());

        return post(response);
    }

    public Future<Void> update(
            final String message,
            final ActionRow... components) {

        final JsonObject patchData = new JsonObject();
        patchData.put("content", message);
        patchData.put("components", Arrays.asList(components));

        final Promise<Void> handle = Promise.promise();
        final Future<HttpResponse<Buffer>> responseFuture;

        responseFuture = webClient.patch(getOriginalMessageEndpoint())
                .sendBuffer(Buffer.buffer(patchData.encode()));

        responseFuture
                .onSuccess(result -> handle.complete())
                .onFailure(err -> {
                    logger.error("Failed: ", err);
                    handle.fail(err);
                });

        return handle.future();
    }

    private Future<Void> post(
            final InteractionResponse<? extends JsonSerializable> callback) {

        final Promise<Void> handle = Promise.promise();
        final Future<HttpResponse<Buffer>> responseFuture;

        logger.debug("POST\n {}", callback.toJson().encodePrettily());
        responseFuture = webClient.post(getCallbackEndpoint())
                .sendBuffer(Buffer.buffer(callback.toJson().encode()));

        responseFuture
                .onSuccess(result -> handle.complete())
                .onFailure(err -> {
                    logger.error("Failed: ", err);
                    handle.fail(err);
                });

        return handle.future();
    }
}
