package net.jimj.discord.agent.interactions;

import io.vertx.core.json.JsonObject;

/**
 * The ordinal value matches the integer value in the API documentation.
 * UNKNOWN is a placeholder for 0, which is unused.
 */
public enum CommandOptionType {
    UNKNOWN, SUB_COMMAND, SUB_COMMAND_GROUP, STRING, INT, BOOL;

    static CommandOptionType fromJson(final JsonObject json) {
        final int type = json.getInteger("type");
        return values()[type];
    }
}
