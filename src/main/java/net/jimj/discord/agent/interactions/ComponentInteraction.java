package net.jimj.discord.agent.interactions;

import io.vertx.core.json.JsonObject;
import net.jimj.discord.agent.response.StatefulComponent;

public interface ComponentInteraction extends Interaction {
    void onInvoke(StatefulComponent component, InteractionContext interactionContext, JsonObject componentData);
}
