package net.jimj.discord.agent.interactions;

/**
 * The ordinal value matches the integer value in the API documentation.
 * UNKNOWN is a placeholder for 0, which is unused.
 */
public enum InteractionType {
    UNKNOWN, PING, APPLICATION_COMMAND, MESSAGE_COMPONENT, APPLICATION_COMMAND_AUTOCOMPLETE, MODAL_SUBMIT
}
