package net.jimj.discord.agent;

import org.immutables.value.Value;

@Value.Immutable
public interface UserContext {
    String snowflake();
    String name();
    UserPreferences preferences();
}
