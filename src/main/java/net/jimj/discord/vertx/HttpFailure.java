package net.jimj.discord.vertx;

public class HttpFailure extends Exception {
    private final int statusCode;

    public HttpFailure(
            final int statusCode,
            final String message) {
        super(message);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public boolean isNotFound() {
        return statusCode == 404;
    }

    public boolean isRedirect() {
        return statusCode >= 300 && statusCode < 400;
    }

    public boolean isClientError() {
        return statusCode >= 400 && statusCode < 500;
    }

    public boolean isRetryable() {
        return statusCode >= 500;
    }
}
