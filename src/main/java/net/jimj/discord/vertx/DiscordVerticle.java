package net.jimj.discord.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.WebSocket;
import io.vertx.core.http.WebsocketVersion;
import io.vertx.core.json.JsonObject;
import net.jimj.discord.agent.DiscordAgent;
import net.jimj.discord.agent.SessionVerticle;
import net.jimj.discord.op.Dispatch;
import net.jimj.discord.op.Hello;
import net.jimj.discord.op.Identify;
import net.jimj.discord.op.OpCode;
import net.jimj.discord.op.Ping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;

public class DiscordVerticle extends AbstractVerticle {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final Promise<DiscordAgent> agentPromise = Promise.promise();
    private final String authToken;

    private WebSocket webSocket;
    private MessageConsumer<JsonObject> eventConsumer;
    private DiscordWebClient discordClient;

    public DiscordVerticle(
            final String authToken) {
        this.authToken = authToken;

    }

    @Override
    public void init(
            final Vertx vertx,
            final Context context) {
        super.init(vertx, context);

        discordClient = new DiscordWebClient(vertx, authToken);

        log.debug("Init Discord Verticles");
        final SessionVerticle session = new SessionVerticle();
        vertx.deployVerticle(session);

        vertx.deployVerticle(new Hello());
        vertx.deployVerticle(new Identify(authToken));
        vertx.deployVerticle(new Ping(this::closeHandler));
        vertx.deployVerticle(new Dispatch());

        agentPromise.complete(new DiscordAgent(vertx, discordClient, session));
    }

    public Future<DiscordAgent> agent() {
        return agentPromise.future();
    }

    @Override
    public void start(
            final Promise<Void> startPromise) {

        connectSocket()
                .onSuccess(ignored -> startPromise.complete())
                .onFailure(startPromise::fail);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        log.info("Stopping");
        webSocket.closeHandler(ignored -> {});
        webSocket.close();
    }

    private Future<WebSocket> connectSocket() {
        return discordClient.get("/gateway/bot")
                .send()
                .compose(response -> Future.succeededFuture(response.bodyAsJsonObject().getString("url")))
                .compose(discordEndpoint -> {
                    final HttpClientOptions options = new HttpClientOptions()
                            .setLogActivity(true)
                            .setMaxWebSocketFrameSize(Integer.MAX_VALUE);
                    final MultiMap headers = MultiMap.caseInsensitiveMultiMap()
                            .add("User-Agent", "DiscordBot (www.jimj.net 0.1)");

                    return vertx.createHttpClient(options)
                            .webSocketAbs(discordEndpoint, headers, WebsocketVersion.V08, Collections.emptyList())
                            .onSuccess(this::initWebSocketHandlers);
                })
                .onFailure(err -> {
                    log.error("Failed to fetch gateway information.", err);
                });
    }

    private void initWebSocketHandlers(final WebSocket webSocket) {
        this.webSocket = webSocket;
        this.eventConsumer = vertx.eventBus()
                .consumer("discord.gateway", (message) -> {
                    final String payload = message.body().encode();
                    log.debug("Sending Payload: {}", payload);
                    webSocket.write(Buffer.buffer(payload));
                });

        webSocket.textMessageHandler(this::opHandler);
        webSocket.closeHandler(ignored -> closeHandler());
    }

    private void opHandler(
            final String json) {

        final JsonObject event = new JsonObject(json);
        log.debug("Received Payload: {}", event);

        try {
            final OpCode opCode = OpCode.find(event.getInteger("op"));

            //Dispatch gets the entire event because it needs the type & sequence
            final JsonObject data = opCode == OpCode.DISPATCH
                    ? event
                    : event.getJsonObject("d", new JsonObject());

            vertx.eventBus().publish(opCode.address(), data);
        } catch (final IllegalArgumentException e) {
            log.warn("Unhandled op code: {}", event);
        }
    }

    private Void closeHandler() {
        log.info("Reconnecting after {} CLOSE received: {}", webSocket.closeStatusCode(), webSocket.closeReason());
        eventConsumer.unregister();
        connectSocket();

        return null;
    }
}
