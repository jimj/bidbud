package net.jimj.discord.vertx;

import io.vertx.core.Vertx;

import java.time.Duration;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EntryExpiringMap<K, V> extends AbstractMap<K, V> {
    private final Map<K, V> backingMap = new HashMap<>();
    private final List<TimedEntry> entryTimes = new ArrayList<>();
    private final long timeToLive;

    public EntryExpiringMap(
            final Vertx vertx,
            final Duration timeToLive,
            final Duration evictionPeriod) {
        this.timeToLive = timeToLive.toMillis();

        vertx.setPeriodic(
                evictionPeriod.toMillis(),
                (timer) -> this.removeStale());
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return backingMap.entrySet();
    }

    @Override
    public V put(final K key, final V value) {
        synchronized (entryTimes) {
            entryTimes.add(new TimedEntry(key));
            return backingMap.put(key, value);
        }
    }

    private void removeStale() {
        final long cutoff = System.currentTimeMillis();

        synchronized (entryTimes) {
            while (!entryTimes.isEmpty() && entryTimes.get(0).expiration <= cutoff) {
                final TimedEntry staleEntry = entryTimes.remove(0);
                backingMap.remove(staleEntry.key);
            }
        }
    }

    private class TimedEntry {
        final K key;
        final long expiration;

        TimedEntry(K key) {
            this.key = key;
            this.expiration = System.currentTimeMillis() + timeToLive;
        }
    }
}
