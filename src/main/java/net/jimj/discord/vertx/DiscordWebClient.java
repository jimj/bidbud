package net.jimj.discord.vertx;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.client.predicate.ResponsePredicate;

public class DiscordWebClient {
    private static final String API_ENDPOINT = "/api/v10";
    private static final ResponsePredicate SUCCESS = ResponsePredicate.create(
            ResponsePredicate.SC_SUCCESS,
            (err) -> new HttpFailure(err.response().statusCode(), err.response().statusMessage() + " - " + err.response().bodyAsString()));

    private final WebClient webClient;
    private final String authToken;

    DiscordWebClient(
            final Vertx vertx,
            final String authToken) {
        final WebClientOptions discordOptions = new WebClientOptions()
                .setDefaultHost("discord.com")
                .setDefaultPort(443)
                .setSsl(true);

        this.webClient = WebClient.create(vertx, discordOptions);
        this.authToken = authToken;
    }

    public HttpRequest<Buffer> get(final String endpoint) {
        return decorate(webClient.get(API_ENDPOINT + endpoint));
    }

    public HttpRequest<Buffer> post(final String endpoint) {
        return decorateForInput(webClient.post(API_ENDPOINT + endpoint));
    }

    public HttpRequest<Buffer> patch(final String endpoint) {
        return decorateForInput(webClient.patch(API_ENDPOINT + endpoint));
    }

    public HttpRequest<Buffer> delete(final String endpoint) {
        return decorate(webClient.delete(API_ENDPOINT + endpoint));
    }

    private <T> HttpRequest<T> decorate(
            final HttpRequest<T> request) {
        return request
                .expect(SUCCESS)
                .putHeader("Authorization", "Bot " + authToken)
                .putHeader("User-Agent", "DiscordBot (www.jimj.net 0.1)");
    }

    private <T> HttpRequest<T> decorateForInput(
            final HttpRequest<T> request) {
        return decorate(request)
                .putHeader("Content-Type", "application/json");
    }
}
