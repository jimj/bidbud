#!/usr/bin/env bash

set -x

# required environment variables: DISCORD_TOKEN, APPLICATION_ID, COMMAND_ID, ROLE_ID
# optional environment variables: GUILD_ID

ENDPOINT="https://discord.com/api/v8/applications/${APPLICATION_ID}"

if [ ! -z "${GUILD_ID}" ]; then
  ENDPOINT="${ENDPOINT}/guilds/${GUILD_ID}"
fi

ENDPOINT="${ENDPOINT}/commands/${COMMAND_ID}/permissions"

data="{\"permissions\":[{\"id\": ${ROLE_ID}, \"type\": 1, \"permission\": true}]}"

echo "POSTing API definition to ${ENDPOINT}"

curl -X PUT \
  -H "Authorization: Bot ${DISCORD_TOKEN}" \
  -H "Content-Type: application/json" \
  -d "${data}" \
  ${ENDPOINT}
