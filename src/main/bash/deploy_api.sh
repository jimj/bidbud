#!/usr/bin/env bash

# required environment variables: DISCORD_TOKEN, APPLICATION_ID
# optional environment variables: GUILD_ID

ENDPOINT="https://discord.com/api/v8/applications/${APPLICATION_ID}"

if [ ! -z "${GUILD_ID}" ]; then
  ENDPOINT="${ENDPOINT}/guilds/${GUILD_ID}"
fi

ENDPOINT="${ENDPOINT}/commands"

echo "POSTing API definition to ${ENDPOINT}"

curl -X POST \
  -H "Authorization: Bot ${DISCORD_TOKEN}" \
  -H "Content-Type: application/json" \
  --data "@${1}" \
  ${ENDPOINT}
