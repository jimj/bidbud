package net.jimj.discord.vertx;

import io.vertx.core.Vertx;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

class EntryExpiringMapTest {
    @Test
    @Timeout(10)
    public void mustExpireStaleEntry() throws Exception {
        final Vertx vertx = Vertx.vertx();
        final Duration expectedTTL = Duration.ofSeconds(3);
        final Map<String, Boolean> expiringMap = new EntryExpiringMap<>(vertx, expectedTTL, Duration.ofSeconds(1));

        final String expectedKey = UUID.randomUUID().toString();
        expiringMap.put(expectedKey, Boolean.TRUE);

        Assertions.assertTrue(expiringMap.containsKey(expectedKey));

        final CountDownLatch assertionLatch = new CountDownLatch(1);

        // After the TTL has expired fire a task to assert that the map no longer contains the expired key.
        vertx.setTimer(
                expectedTTL.plus(1, ChronoUnit.SECONDS).toMillis(),
                (timer) -> {
                    Assertions.assertFalse(expiringMap.containsKey(expectedKey));
                    assertionLatch.countDown();
                });

        assertionLatch.await();
    }
}