package net.jimj.bidbud.dkp;

import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

class InputParserTest {
    private JsonObject givenAPrivateMessage(final String content) {
        return new JsonObject()
                .put("author", new JsonObject().put("id", UUID.randomUUID().toString()))
                .put("content", content);
    }

    private JsonObject givenAPublicMessage(final String content) {
        return givenAPrivateMessage(content)
                .put("guild_id", "1234567890");
    }

    private static Stream<Arguments> providePositiveChecks() {
        return Stream.of(
                Arguments.of(".dkp", ""),
                Arguments.of(".DKP", ""),
                Arguments.of("dkp toon", "toon"),
                Arguments.of(".dkp toon", "toon"),
                Arguments.of("toon dkp", "toon"),
                Arguments.of("toon .dkp", "toon"),
                Arguments.of("DKP toon", "toon"),
                Arguments.of(".DKP toon", "toon"),
                Arguments.of("toon DKP", "toon"),
                Arguments.of("toon .DKP", "toon"));
    }

    private static Stream<Arguments> provideNegativeChecks() {
        return Stream.of(
                Arguments.of("toon"),
                Arguments.of("what is toon's dkp"),
                Arguments.of("When is the next dkp raid?"),
                Arguments.of("Just use .dkp character"));
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("providePositiveChecks")
    public void testLooksLikeDKPCheck(
            final String content,
            final String ignored) {

        Assertions.assertTrue(InputParser.looksLikeDKPCheck(content));
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("providePositiveChecks")
    public void testParseProvidesCharacter(
            final String content,
            final String expectedCharacter) {

        final Optional<String> expected = expectedCharacter.equals("")
                ? Optional.empty()
                : Optional.of(expectedCharacter);

        Assertions.assertEquals(expected, InputParser.parseCharacterName(content));
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("provideNegativeChecks")
    public void testDoesNotLooksLikeDKPCheck(
            final String content) {

        Assertions.assertFalse(InputParser.looksLikeDKPCheck(content));
    }
}