package guilds.disciples;

import io.vertx.core.json.JsonObject;
import net.jimj.discord.agent.ImmutableMessageContext;
import net.jimj.discord.agent.ImmutableUserContext;
import net.jimj.discord.agent.MessageContext;
import net.jimj.discord.agent.TextChannelFacade;
import net.jimj.discord.agent.UserContext;
import net.jimj.discord.agent.UserPreferences;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.UUID;
import java.util.stream.Stream;

public class AuctionParserSimplifiedBidTest {
    private static Stream<Arguments> provideItemNames() {
        return Stream.of(
                Arguments.of("A sword"),
                Arguments.of("apostrophe's revenge"),
                Arguments.of("Comma, the silent destroyer"),
                Arguments.of("The Final Item."),
                Arguments.of("d-a-s-h"),
                Arguments.of("This item; 1 of a kind: Named-awfully, t`break your code nerd!"));
    }

    private MessageContext givenAPrivateMessage(final String content, final JsonObject userPreferences) {
        final UserContext userContext = ImmutableUserContext.builder()
                .snowflake(UUID.randomUUID().toString())
                .name("user#0000")
                .preferences(new UserPreferences(userPreferences))
                .build();

        return ImmutableMessageContext.builder()
                .message(content)
                .userContext(userContext)
                .channel(Mockito.mock(TextChannelFacade.class))
                .build();
    }

    @ParameterizedTest
    @MethodSource("provideItemNames")
    public void testCanBidWithQuotes(
            final String expectedItem) throws Exception {

        final String bidString = String.format("\"%s\" 10", expectedItem);
        Assertions.assertTrue(AuctionParser.looksLikeSimplifiedBid(bidString));

        final String expectedName = UUID.randomUUID().toString();
        final AuctionParser.ParsedBid actualBid = AuctionParser.parseSimplifiedBid(
                givenAPrivateMessage(bidString, new JsonObject().put("mainRaider", expectedName)));

        Assertions.assertEquals(expectedItem, actualBid.item());
        Assertions.assertEquals(expectedName, actualBid.bid().characterName());
    }

    @ParameterizedTest
    @MethodSource("provideItemNames")
    public void testCanBidWithSmartQuotes(
            final String expectedItem) throws Exception {

        final String bidString = String.format("“%s“ 10", expectedItem);
        Assertions.assertTrue(AuctionParser.looksLikeSimplifiedBid(bidString));

        final String expectedName = UUID.randomUUID().toString();
        final AuctionParser.ParsedBid actualBid = AuctionParser.parseSimplifiedBid(
                givenAPrivateMessage(bidString, new JsonObject().put("mainRaider", expectedName)));

        Assertions.assertEquals(expectedItem, actualBid.item());
        Assertions.assertEquals(expectedName, actualBid.bid().characterName());
    }

    @ParameterizedTest
    @MethodSource("provideItemNames")
    public void testCanBidWithoutQuotes(
            final String expectedItem) throws Exception {

        final String bidString = String.format("%s 10", expectedItem);
        Assertions.assertTrue(AuctionParser.looksLikeSimplifiedBid(bidString));

        final String expectedName = UUID.randomUUID().toString();
        final AuctionParser.ParsedBid actualBid = AuctionParser.parseSimplifiedBid(
                givenAPrivateMessage(bidString, new JsonObject().put("mainRaider", expectedName)));

        Assertions.assertEquals(expectedItem, actualBid.item());
        Assertions.assertEquals(expectedName, actualBid.bid().characterName());
    }

    @ParameterizedTest
    @MethodSource("provideItemNames")
    public void testBiddingToleratesExtraWhitespace(
            final String expectedItem) throws Exception {

        final String bidString = String.format("%s   10", expectedItem);
        Assertions.assertTrue(AuctionParser.looksLikeSimplifiedBid(bidString));

        final String expectedName = UUID.randomUUID().toString();
        final AuctionParser.ParsedBid actualBid = AuctionParser.parseSimplifiedBid(
                givenAPrivateMessage(bidString, new JsonObject().put("mainRaider", expectedName)));

        Assertions.assertEquals(expectedItem, actualBid.item());
        Assertions.assertEquals(expectedName, actualBid.bid().characterName());
    }
}
