package guilds.disciples;

import net.jimj.bidbud.auction.Bid;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.UUID;
import java.util.stream.Stream;

import static guilds.disciples.GuildConstants.Rank.ALT;
import static guilds.disciples.GuildConstants.Rank.BOX;
import static guilds.disciples.GuildConstants.Rank.CRITICAL;
import static guilds.disciples.GuildConstants.Rank.RAIDER;

public class BidComparatorTest {
    private static final int EQUAL = 0;
    private static final int LESS_THAN = 1;
    private static final int GREATER_THAN = -1;

    private static Bid<BidContext> givenABid(
            final int amount,
            final GuildConstants.Rank rank) {
        return new Bid<>(UUID.randomUUID().toString(), UUID.randomUUID().toString(), amount, new BidContext(rank));
    }

    private static Stream<Arguments> provideBasicComparisons() {
        return Stream.of(
                Arguments.of(givenABid(50, RAIDER), EQUAL, givenABid(50, RAIDER)),
                Arguments.of(givenABid(50, RAIDER), LESS_THAN, givenABid(75, RAIDER)),
                Arguments.of(givenABid(50, RAIDER), GREATER_THAN, givenABid(25, RAIDER)),
                Arguments.of(givenABid(500, RAIDER), EQUAL, givenABid(500, CRITICAL)),

                Arguments.of(givenABid(100, BOX), GREATER_THAN, givenABid(99, RAIDER)),
                Arguments.of(givenABid(100, BOX), EQUAL, givenABid(100, RAIDER)),
                Arguments.of(givenABid(101, BOX), LESS_THAN, givenABid(101, RAIDER)),
                Arguments.of(givenABid(500, BOX), LESS_THAN, givenABid(101, RAIDER)),
                Arguments.of(givenABid(100, BOX), GREATER_THAN, givenABid(99, CRITICAL)),
                Arguments.of(givenABid(100, BOX), EQUAL, givenABid(100, CRITICAL)),
                Arguments.of(givenABid(101, BOX), LESS_THAN, givenABid(101, CRITICAL)),
                Arguments.of(givenABid(500, BOX), LESS_THAN, givenABid(101, CRITICAL)),

                Arguments.of(givenABid(500, ALT), LESS_THAN, givenABid(1, RAIDER)),
                Arguments.of(givenABid(500, ALT), LESS_THAN, givenABid(1, BOX)),
                Arguments.of(givenABid(500, ALT), LESS_THAN, givenABid(1, CRITICAL))
        );
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("provideBasicComparisons")
    public void testComparison(
            final Bid<BidContext> first,
            final int expected,
            final Bid<BidContext> second) {

        final int actual = new BidComparator().compare(first, second);
        Assertions.assertEquals(expected, actual);
    }
}
