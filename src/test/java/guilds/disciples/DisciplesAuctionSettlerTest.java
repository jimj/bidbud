package guilds.disciples;

import net.jimj.bidbud.auction.Auction;
import net.jimj.bidbud.auction.Bid;
import net.jimj.bidbud.auction.Loot;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static guilds.disciples.GuildConstants.Rank.ALT;
import static guilds.disciples.GuildConstants.Rank.BOX;
import static guilds.disciples.GuildConstants.Rank.CRITICAL;
import static guilds.disciples.GuildConstants.Rank.RAIDER;

class DisciplesAuctionSettlerTest {
    private final DisciplesAuctionSettler disciplesAuctionSettler = new DisciplesAuctionSettler();

    private Bid<BidContext> givenABid(
            final int amount,
            final GuildConstants.Rank rank) {
        return new Bid<>(UUID.randomUUID().toString(), UUID.randomUUID().toString(), amount, new BidContext(rank));
    }

    private Auction<BidContext> givenAnAuction(
            final int quantity,
            final Bid<BidContext>... bids) {
        return new Auction<>(new Loot(UUID.randomUUID().toString(), quantity), Arrays.asList(bids));
    }

    private SettlementAssertions whenSettled(final Auction<BidContext> auction) {
        return new SettlementAssertions(disciplesAuctionSettler.settle(auction));
    }

    @Test
    public void testNoBids() {
        final SettlementAssertions assertions = whenSettled(givenAnAuction(1));

        assertions.assertWinnerCount(0)
                .assertPriceSetterMatches(bid -> bid.amount() == 0);
    }

    @Test
    public void testSingleBid() {
        final Auction<BidContext> auction = givenAnAuction(1, givenABid(50, RAIDER));
        final SettlementAssertions assertions = whenSettled(auction);

        assertions.assertWinnerCount(1)
                .assertAnyWinnerMatches(bid -> bid.amount() == 1);
    }

    @Test
    public void testMoreItemsThanBids() {
        final Auction<BidContext> auction = givenAnAuction(2, givenABid(50, RAIDER));
        final SettlementAssertions assertions = whenSettled(auction);

        assertions.assertWinnerCount(1)
                .assertAnyWinnerMatches(bid -> bid.amount() == 1);
    }

    @Test
    public void testMultipleItems() {
        final Auction<BidContext> auction = givenAnAuction(2,
                givenABid(50, RAIDER),
                givenABid(75, RAIDER),
                givenABid(20, RAIDER));
        final SettlementAssertions assertions = whenSettled(auction);

        assertions.assertWinnerCount(2)
                .assertAllWinnersMatches(bid -> bid.amount() == 21);
    }

    @Test
    public void testSingleItemMultipleBiddersClearWinner() {
        final Auction<BidContext> auction = givenAnAuction(1,
                givenABid(30, RAIDER),
                givenABid(10, RAIDER),
                givenABid(50, RAIDER));

        final SettlementAssertions assertions = whenSettled(auction);

        assertions.assertWinnerCount(1)
                .assertAllWinnersMatches(bid -> bid.amount() == 31);
    }

    @Test
    public void testSingleItemMultipleBiddersBoxLosesWithCappedBid() {
        final Auction<BidContext> auction = givenAnAuction(1,
                givenABid(500, RAIDER),
                givenABid(300, BOX));

        final SettlementAssertions assertions = whenSettled(auction);
        assertions.assertWinnerCount(1)
                .assertAllWinnersMatches(bid -> bid.amount() == BOX.getCap() + 1);
    }

    @Test
    public void testMultipleWinnersMixedPricing() {
        final Auction<BidContext> auction = givenAnAuction(2,
                givenABid(150, RAIDER),
                givenABid(200, BOX),
                givenABid(205, BOX));

        final SettlementAssertions assertions = whenSettled(auction);

        assertions
                .assertWinnerCountMatching(1, bid -> bid.context().bidderRank() == RAIDER && bid.amount() == 101)
                .assertWinnerCountMatching(1, bid -> bid.context().bidderRank() == BOX && bid.amount() == 603);
    }

    @Test
    public void testMultipleWinnersMixedPricingBoxWinners() {
        final Auction<BidContext> auction = givenAnAuction(3,
                givenABid(90, RAIDER),
                givenABid(30, RAIDER),
                givenABid(50, BOX),
                givenABid(205, BOX));

        final SettlementAssertions assertions = whenSettled(auction);

        assertions
                .assertWinnerCountMatching(1, bid -> bid.context().bidderRank() == RAIDER && bid.amount() == 31)
                .assertWinnerCountMatching(2, bid -> bid.context().bidderRank() == BOX && bid.amount() == 93);
    }

    @Test
    public void testTieIsRandomlyDetermined() {
        final Auction<BidContext> auction = givenAnAuction(1,
                givenABid(150, RAIDER),
                givenABid(150, RAIDER),
                givenABid(150, RAIDER),
                givenABid(150, RAIDER));

        final Set<String> seenWinners = new HashSet<>();

        for (int i = 0; i < 50; i++) {
            final SettlementAssertions assertions = whenSettled(auction);
            assertions.assertWinnerCountMatching(1, bid -> bid.amount() == 150);
            assertions.winners().forEach(winner -> seenWinners.add(winner.characterName()));
        }

        Assertions.assertEquals(4, seenWinners.size());
    }

    @Test
    public void testMultipleItemsSecondWinnerTieRandomlyDetermined() {
        final Bid<BidContext> expectedWinner = givenABid(150, RAIDER);
        final Auction<BidContext> auction = givenAnAuction(2,
                givenABid(140, RAIDER),
                givenABid(140, RAIDER),
                givenABid(140, RAIDER),
                expectedWinner);

        final Set<String> seenWinners = new HashSet<>();

        for (int i = 0; i < 50; i++) {
            final SettlementAssertions assertions = whenSettled(auction);
            assertions
                    .assertWinnerCountMatching(1, bid -> bid.characterName().equals(expectedWinner.characterName()) && bid.amount() == 141)
                    .assertWinnerCountMatching(1, bid -> bid.amount() == 140);
            assertions.winners()
                    .filter(bid -> !bid.characterName().equals(expectedWinner.characterName()))
                    .forEach(winner -> seenWinners.add(winner.characterName()));
        }

        Assertions.assertEquals(3, seenWinners.size());
    }

    @Test
    public void testMultipleWinnersSecondPlaceIsBoxesOnly() {
        final Auction<BidContext> auction = givenAnAuction(2,
                givenABid(105, RAIDER),
                givenABid(300, BOX),
                givenABid(250, BOX),
                givenABid(75, RAIDER));

        final SettlementAssertions assertions = whenSettled(auction);

        assertions
                .assertWinnerCountMatching(1, bid -> bid.context().bidderRank() == RAIDER && bid.amount() == 101)
                .assertWinnerCountMatching(1, bid -> bid.context().bidderRank() == BOX && bid.amount() == 753);
    }

    @ParameterizedTest
    @EnumSource(value = GuildConstants.Rank.class, names = "ALT", mode = EnumSource.Mode.EXCLUDE)
    public void testAltsAlwaysLoseWhenOthersBid(
            final GuildConstants.Rank otherRank) {
        final Auction<BidContext> auction = givenAnAuction(1,
                givenABid(500, ALT),
                givenABid(1, otherRank));
        final SettlementAssertions assertions = whenSettled(auction);

        assertions.assertWinnerCount(1)
                .assertAllWinnersMatches(winningBid -> winningBid.context().bidderRank() == otherRank);
    }

    @ParameterizedTest
    @EnumSource(value = GuildConstants.Rank.class, names = {"BOX", "ALT"}, mode = EnumSource.Mode.EXCLUDE)
    public void testBoxPaysFullPriceAgainstOtherRanks(
            final GuildConstants.Rank otherRank) {
        final Auction<BidContext> auction = givenAnAuction(1,
                givenABid(500, BOX),
                givenABid(99, otherRank));
        final SettlementAssertions assertions = whenSettled(auction);

        assertions.assertWinnerCount(1)
                .assertAllWinnersMatches(bid -> bid.context().bidderRank() == BOX)
                .assertAllWinnersMatches(bid -> bid.amount() == 100 * BOX.getMultiplier());
    }

    @Test
    public void testCriticalPaysFullPriceAgainstRaider() {
        final Auction<BidContext> auction = givenAnAuction(1,
                givenABid(200, CRITICAL),
                givenABid(150, RAIDER));
        final SettlementAssertions assertions = whenSettled(auction);

        assertions.assertWinnerCount(1)
                .assertAllWinnersMatches(bid -> bid.context().bidderRank() == CRITICAL)
                .assertAllWinnersMatches(bid -> bid.amount() == 151 * CRITICAL.getMultiplier());
    }

    @Test
    public void testCriticalPaysCapAgainstBox() {
        final Auction<BidContext> auction = givenAnAuction(1,
                givenABid(200, CRITICAL),
                givenABid(150, BOX));
        final SettlementAssertions assertions = whenSettled(auction);

        assertions.assertWinnerCount(1)
                .assertAllWinnersMatches(bid -> bid.context().bidderRank() == CRITICAL)
                .assertAllWinnersMatches(bid -> bid.amount() == (BOX.getCap() + 1) * CRITICAL.getMultiplier());
    }

    @Test
    public void bidsContainsAllBidders() {
        final Auction<BidContext> auction = givenAnAuction(2,
                givenABid(105, RAIDER),
                givenABid(250, BOX),
                givenABid(250, BOX),
                givenABid(75, RAIDER));

        final SettlementAssertions assertions = whenSettled(auction);

        assertions
                .assertSettledAuction(settledAuction -> settledAuction.bids().size() == auction.getBids().size());
    }
}