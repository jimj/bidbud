package guilds.disciples;

import net.jimj.discord.agent.ImmutableMessageContext;
import net.jimj.discord.agent.ImmutableUserContext;
import net.jimj.discord.agent.MessageContext;
import net.jimj.discord.agent.TextChannelFacade;
import net.jimj.discord.agent.UserContext;
import net.jimj.discord.agent.UserPreferences;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.UUID;
import java.util.stream.Stream;

public class AuctionParserBidTest {
    private static Stream<Arguments> provideItemNames() {
        return Stream.of(
                Arguments.of("A sword"),
                Arguments.of("apostrophe's revenge"),
                Arguments.of("Comma, the silent destroyer"),
                Arguments.of("The Final Item."),
                Arguments.of("d-a-s-h"),
                Arguments.of("This item; 1 of a kind: Named-awfully, t`break your code nerd!"));
    }

    private static Stream<Arguments> provideNegativeBidCases() {
        return Stream.of(
                Arguments.of("hello"),
                Arguments.of("foo toon 10 typorank"),
                Arguments.of("Loot 10"),
                Arguments.of("Multi-word Loot 10"));
    }

    private MessageContext givenAPrivateMessage(final String content) {
        final UserContext userContext = ImmutableUserContext.builder()
                .snowflake(UUID.randomUUID().toString())
                .name("user#0000")
                .preferences(UserPreferences.defaultPreferences())
                .build();

        return ImmutableMessageContext.builder()
                .message(content)
                .userContext(userContext)
                .channel(Mockito.mock(TextChannelFacade.class))
                .build();
    }

    @ParameterizedTest
    @MethodSource("provideItemNames")
    public void testCanBidWithQuotes(
            final String expectedItem) throws Exception {

        final String bidString = String.format("\"%s\" toon 10 raider", expectedItem);

        Assertions.assertTrue(AuctionParser.looksLikeBid(bidString));

        final String actualItem = AuctionParser.parseBid(givenAPrivateMessage(bidString)).item();

        Assertions.assertEquals(expectedItem, actualItem);
    }

    @ParameterizedTest
    @MethodSource("provideItemNames")
    public void testCanBidWithSmartQuotes(
            final String expectedItem) throws Exception {

        final String bidString = String.format("“%s“ toon 10 raider", expectedItem);

        Assertions.assertTrue(AuctionParser.looksLikeBid(bidString));

        final String actualItem = AuctionParser.parseBid(givenAPrivateMessage(bidString)).item();

        Assertions.assertEquals(expectedItem, actualItem);
    }

    @ParameterizedTest
    @MethodSource("provideItemNames")
    public void testCanBidWithoutQuotes(
            final String expectedItem) throws Exception {

        final String bidString = String.format("%s toon 10 raider", expectedItem);

        Assertions.assertTrue(AuctionParser.looksLikeBid(bidString));

        final String actualItem = AuctionParser.parseBid(givenAPrivateMessage(bidString)).item();

        Assertions.assertEquals(expectedItem, actualItem);
    }

    @ParameterizedTest
    @MethodSource("provideItemNames")
    public void testBiddingToleratesExtraWhitespace(
            final String expectedItem) throws Exception {

        final String bidString = String.format("%s   toon  10   raider", expectedItem);

        Assertions.assertTrue(AuctionParser.looksLikeBid(bidString));

        final AuctionParser.ParsedBid actualBid = AuctionParser.parseBid(givenAPrivateMessage(bidString));

        Assertions.assertEquals(expectedItem, actualBid.item());
        Assertions.assertEquals("toon", actualBid.bid().characterName());
        Assertions.assertNotNull(actualBid.bid());
    }

    @ParameterizedTest
    @EnumSource(value = GuildConstants.Rank.class)
    public void testBidSupportsAllRanks(
            final GuildConstants.Rank expectedRank) throws Exception {
        final String auctionFormat = "item toon 1 %s";

        final String lowerCase = String.format(auctionFormat, expectedRank.toString().toLowerCase());
        Assertions.assertTrue(AuctionParser.looksLikeBid(lowerCase));

        final String upperCase = String.format(auctionFormat, expectedRank);
        Assertions.assertTrue(AuctionParser.looksLikeBid(upperCase));
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("provideNegativeBidCases")
    public void testParseBidDefensiveness(
            final String content) {

        Assertions.assertThrows(
                AuctionParser.ParseException.class,
                () -> AuctionParser.parseBid(givenAPrivateMessage(content)));
    }
}
