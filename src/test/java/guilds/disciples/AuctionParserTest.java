package guilds.disciples;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

class AuctionParserTest {
    private static Stream<Arguments> provideStartAuctionCases() {
        return Stream.of(
                Arguments.of(".dkp startbids \"foo\"", "foo", 1),
                Arguments.of(".dkp startbids  \"extra spaces\"", "extra spaces", 1),
                Arguments.of(".dkp startbids  2x  \"extra spaces\"", "extra spaces", 2),
                Arguments.of(".dkp startbids “Smart Quotes\"", "Smart Quotes", 1),
                Arguments.of(".dkp startbids 4 \"foo\"", "foo", 4),
                Arguments.of(".dkp startbids 4x \"foo\"", "foo", 4),
                Arguments.of(".dkp startbids 3 “Smart Quotes“", "Smart Quotes", 3),
                Arguments.of(".dkp startbids 3x “Smart Quotes“", "Smart Quotes", 3),
                Arguments.of(".dkp startbids \"Gyro Core: \"Martialism\"", "Gyro Core: Martialism", 1));
    }

    private static Stream<Arguments> provideCancelAuctionCases() {
        return Stream.of(
                Arguments.of(".dkp cancel \"foo\"", "foo"),
                Arguments.of(".dkp cancel “Smart Quotes“", "Smart Quotes")
        );
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("provideStartAuctionCases")
    public void testLooksLikeAuction(
            final String content,
            final String ignoredName,
            final int ignoredQuantity) {

        Assertions.assertTrue(AuctionParser.looksLikeAuction(content));
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("provideStartAuctionCases")
    public void testLooksLikeAuctionWhenTestingBids(
            final String content,
            final String ignoredName,
            final int ignoredQuantity) {

        final String testBidContent = content.replaceAll("start", "test");
        Assertions.assertTrue(AuctionParser.looksLikeAuction(testBidContent));
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("provideStartAuctionCases")
    public void testStartAuctionParse(
            final String content,
            final String expectedName,
            final int expectedQuantity) throws Exception {

        final AuctionParser.ParsedAuction actualAuction = AuctionParser.parseStart(content);
        Assertions.assertEquals(expectedName, actualAuction.name());
        Assertions.assertEquals(expectedQuantity, actualAuction.quantity());
        Assertions.assertFalse(actualAuction.isTest());
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("provideStartAuctionCases")
    public void testStartAuctionParseWhenTestingBids(
            final String content,
            final String expectedName,
            final int expectedQuantity) throws Exception {

        final String testBidContent = content.replaceAll("start", "test");
        final AuctionParser.ParsedAuction actualAuction = AuctionParser.parseStart(testBidContent);
        Assertions.assertEquals(expectedName, actualAuction.name());
        Assertions.assertEquals(expectedQuantity, actualAuction.quantity());
        Assertions.assertTrue(actualAuction.isTest());
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("provideStartAuctionCases")
    public void testStartAuctionMustProvideZeroDuration(
            final String content) throws Exception {

        final Duration actualDuration = AuctionParser.parseStart(content).duration();
        Assertions.assertEquals(Duration.ZERO, actualDuration);
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("provideStartAuctionCases")
    public void testStartAuctionParsesDuration(
            final String content) throws Exception {
        final int expectedDuration = ThreadLocalRandom.current().nextInt(10);
        final String updatedContent = content + " " + expectedDuration;

        final Duration actualDuration = AuctionParser.parseStart(updatedContent).duration();
        Assertions.assertEquals(expectedDuration, actualDuration.toMinutes());
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("provideCancelAuctionCases")
    public void testLooksLikeCancelAuction(
            final String content,
            final String ignored) {
        Assertions.assertTrue(AuctionParser.looksLikeAuctionCancel(content));
    }

    @ParameterizedTest(name = "[{index}] {0}")
    @MethodSource("provideCancelAuctionCases")
    public void testParseCancelAuction(
            final String content,
            final String expectedAuction) {
        final String actualAuction = AuctionParser.parseCancelAuction(content);
        Assertions.assertEquals(expectedAuction, actualAuction);
    }
}