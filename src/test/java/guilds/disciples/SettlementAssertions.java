package guilds.disciples;

import net.jimj.bidbud.auction.Bid;
import org.junit.jupiter.api.Assertions;

import java.util.function.Predicate;
import java.util.stream.Stream;

public class SettlementAssertions {
    private final SettledAuction settledAuction;

    SettlementAssertions(final SettledAuction settledAuction) {
        this.settledAuction = settledAuction;
    }

    SettlementAssertions assertWinnerCount(
            final int expectedWinnerCount) {
        Assertions.assertEquals(expectedWinnerCount, settledAuction.settlements().size());
        return this;
    }

    SettlementAssertions assertWinnerCountMatching(
            final int expectedWinnerCount,
            final Predicate<Bid<BidContext>> bidPredicate) {
        final long actualCount = settledAuction.settlements().stream()
                .filter(bidPredicate)
                .count();
        Assertions.assertEquals(expectedWinnerCount, actualCount);
        return this;
    }

    SettlementAssertions assertPriceSetterMatches(
            final Predicate<Bid<BidContext>> bidPredicate) {
        Assertions.assertTrue(bidPredicate.test(settledAuction.priceSetter()));
        return this;
    }

    SettlementAssertions assertAnyWinnerMatches(
            final Predicate<Bid<BidContext>> winnerPredicate) {
        Assertions.assertTrue(settledAuction
                .settlements().stream()
                .anyMatch(winnerPredicate));

        return this;
    }

    SettlementAssertions assertAllWinnersMatches(
            final Predicate<Bid<BidContext>> winnerPredicate) {
        Assertions.assertTrue(settledAuction
                .settlements().stream()
                .allMatch(winnerPredicate));

        return this;
    }

    Stream<Bid<BidContext>> winners() {
        return settledAuction.settlements().stream();
    }

    SettlementAssertions assertSettledAuction(
            final Predicate<SettledAuction> auctionPredicate) {
        Assertions.assertTrue(auctionPredicate.test(settledAuction));
        return this;
    }
}
