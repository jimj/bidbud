## Running
The build produces a super-jar with dependencies bundled.
You will need to define some environment variables to make the bot impl run

* `DISCORD_TOKEN` - Generated in the Discord developer portal
* `DKP_TOKEN` - An API token from EQDKP for a user with enough privilege to create raids and items
* `DKP_SITE` - The EQDKP url for the site
* `USER_PREFS_DIR` - Optional.  User preferences will be written here.

## Development
This is ~3 projects smashed into a single codebase.  Documentation is generally missing.  Maven is the build tool.

* net.jimj.discord - A Vert.x based Discord client.  Complete enough to be handy but not comprehensive.  Very little tests.
* net.jimj.bidbud - A DKP management & Auction bot library.  Probably not functional on its own.
* guilds.disciples - A Bot implementation using the above.  Not cleanly broken from bidbud library.

`guilds.disciples.Main` is the primary entry point.
`guilds.discples.GuildConstants` will need to be updated to match your development server's name & channel needs.
